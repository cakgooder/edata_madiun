/* ------------------------------------------------------------------------------
*
*  # All Clicked Button JS
*
*  JS for All Clicked Button in System
*
*  Version: 1.0
*  Latest update: Jul 31, 2017
*
* ---------------------------------------------------------------------------- */
/* START CODE */
/* VARIABEL DECLARATION */
//var group_kolom = GROUP_KOLOM;
var index_kolom_data = INDEX_KOLOM_DATA;
//var index_set_skpd = INDEX_SET_SKPD;

/* FUNCTION DECLARATION */
/* Function tambah kolom group */
function tambah_group_kolom(){
	/* Kirim Data ke Controller */
	$.ajax({
		url: BASE_URL + GOODER_PATH.tambah_kolom_group + group_kolom,
		type: 'POST',
		dataType: 'html',
		beforeSend: function(){
			/* Show Loading */
			$("#loading-tambah-kolom-group").show();
		},
		success: function(result){
			/* Tambah Kolom */
			$("#kolom-group").append(result);

			/* Hide Loading */
			$("#loading-tambah-kolom-group").hide();

			/* group_kolom tambah */
			group_kolom++;
		}
	});
};
/* Function tambah data kolom */
function tambah_data_kolom(){
	/* Kirim Data ke Controller */
	$.ajax({
		url: BASE_URL + GOODER_PATH.tambah_kolom_data + index_kolom_data,
		type: 'POST',
		dataType: 'html',
		beforeSend: function(){
			/* Show Loading */
			$("#loading-tambah-kolom-data").show();
		},
		success: function(result){
			/* Tambah Kolom */
			$("#kolom-data").append(result);

			/* Hide Loading */
			$("#loading-tambah-kolom-data").hide();

			/* data_kolom tambah */
			index_kolom_data++;
			rubah_urutan_kolom();
		}
	});
};
/* Function tambah Set SKPD */
function tambah_set_skpd(id_data_master){
	/* Kirim Data ke Controller */
	$.ajax({
		url: BASE_URL + GOODER_PATH.tambah_set_skpd + index_set_skpd + "/" + id_data_master,
		type: 'POST',
		dataType: 'html',
		beforeSend: function(){
			/* Show Loading */
			$("#loading-tambah-set-skpd").show();
		},
		success: function(result){
			/* Tambah Kolom */
			$("#kolom-set-skpd").append(result);

			/* Hide Loading */
			$("#loading-tambah-set-skpd").hide();

			/* data_kolom tambah */
			index_set_skpd++;
		}
	});
};
/* ACTION DECLARATION */
/* Load panel group kolom */
if ($("h5.panel-title").html() == "Form Tambah Group") {
	tambah_group_kolom();
} else if($("h5.panel-title").html() == "Form Tambah Data Master") {
	tambah_data_kolom();
} else if($("h5.panel-title").html() == "Form Edit Data Master") {
	rubah_urutan_kolom();
}

//$('#modal-cron-job-kegiatan-subtitle').modal();

/* Button MANAJEMEN USER */
/* Button Lock dan Unlock User Show Modal */
$("body").on('click', '.btn-lock-user', function(event) {
	/* Act on the event */
	/* Get data */
	var user_id = $(this).data("user-id");
	var status = $(this).data("status");

	/* Set input */
	$(".input-user-id").val(user_id);
	$(".input-status").val(status);

	if (status == "locked") {
		/* Show modal */
		$("#modal-lock-user").modal("toggle");
	} else if (status == "unlocked") {
		/* Show modal */
		$("#modal-unlock-user").modal("toggle");
	}
});
/* Button Hapus User */
$("body").on('click', '.btn-hapus-user', function(event) {
	/* Act on the event */
	/* Get data */
	var user_id = $(this).data("user-id");

	/* Set input */
	$(".input-user-id").val(user_id);

	/* Show modal */
	$("#modal-hapus-user").modal("toggle");
});
/* END MANAJEMEN USER */

/* Button GROUP HEADER */
/* Button Tambah Kolom Group Header */
$("body").on('click', '#btn-group-tambah-kolom', function(event) {
	event.preventDefault();
	/* Act on the event */
	tambah_group_kolom();
});
/* Button Remove Kolom Group Header */
$("body").on('click', '.btn-remove-kolom', function(event) {
	event.preventDefault();
	/* Act on the event */
	var tobe_removed = $(this).data("tobe-removed");

	/* Set hapus_proses */
	$("input[name='hapus_proses']").val(tobe_removed);

	/* Toggle Modal */
	$("#modal-hapus-kolom").modal("toggle");
});
/* Button Yakin Remove Kolom Group Header */
$("body").on('click', '#btn-yakin-remove-kolom', function(event) {
	event.preventDefault();
	/* Act on the event */
	var tobe_removed = $("input[name='hapus_proses']").val();

	/* Delete Div */
	$(tobe_removed).remove();

	/* Check Urutan Kolom */
	rubah_urutan_kolom();

	/* Toggle Modal */
	$("#modal-hapus-kolom").modal("toggle");
});
/* Button Hapus Group Header */
$("body").on('click', '.btn-hapus-group', function(event) {
	event.preventDefault();
	/* Act on the event */
	var group_id = $(this).data("id-group");

	/* Set input */
	$(".input-id-group-header").val(group_id);

	/* Toggle Modal */
	$("#modal-hapus-group-header").modal("toggle");
});
/* END GROUP HEADER */


/* Button Show Help Upload Excel */
$("body").on('click', '.btn-help-show', function(event) {
	event.preventDefault();
	/* Act on the event */
	var jenis_help = $(this).data("jenis-help");
	$(jenis_help).modal("toggle");
});

/* Button Show Panel Upload Lanjutan */
$("body").on('click', '#btn-show-upload-excel-lanjutan', function(event) {
	event.preventDefault();
	/* Act on the event */
	$(".panel-upload").hide();
	$("#panel-upload-excel-lanjutan").show();
});
/* Button Show Panel Upload Baru */
$("body").on('click', '#btn-show-upload-excel-baru', function(event) {
	event.preventDefault();
	/* Act on the event */
	$(".panel-upload").hide();
	$("#panel-upload-excel-baru").show();
});
/* Button Show Panel No Realisasi */
$("body").on('click', '#btn-show-upload-no-realisasi', function(event) {
	event.preventDefault();
	/* Act on the event */
	$(".panel-upload").hide();
	$("#panel-upload-no-realisasi").show();
});
/* Button Show Panel Upload File */
$("body").on('click', '#btn-show-upload-file', function(event) {
	event.preventDefault();
	/* Act on the event */
	$(".panel-upload").hide();
	$("#panel-upload-excel-baru").show();
});

/* Button Batal Upload */
$("body").on('click', '.btn-batal-upload', function(event) {
	event.preventDefault();
	/* Act on the event */
	setTimeout(function(){ self.close(); }, 50 );
});

/* END REALISASI GET */

/* Button DATA MASTER */
/* Button Hapus Data Master */
$("body").on('click', '.btn-hapus-data-master', function(event) {
	event.preventDefault();
	/* Act on the event */
	var id_data_master = $(this).data("id-data-master");

	/* Set input */
	$(".input-id-data-master").val(id_data_master);

	/* Toggle Modal */
	$("#modal-hapus-data-master").modal("toggle");
});
/* Button Tambah Kolom Data Master */
$("body").on('click', '#btn-data-tambah-kolom', function(event) {
	event.preventDefault();
	/* Act on the event */
	tambah_data_kolom();
});
/* Button Tambah SKPD Data Master */
$("body").on('click', '#btn-tambah-set-skpd', function(event) {
	event.preventDefault();
	/* Act on the event */

	/* Get id */
	id_data_master = $(this).data("id-data-master");

	/* Kirim ke set */
	tambah_set_skpd(id_data_master);
});
/* Button Remove Kolom Group Header */
$("body").on('click', '.btn-remove-set-skpd', function(event) {
	event.preventDefault();
	/* Act on the event */
	var tobe_removed = $(this).data("tobe-removed");

	/* Set hapus_proses */
	$("input[name='hapus_proses']").val(tobe_removed);

	/* Toggle Modal */
	$("#modal-hapus-set-skpd").modal("toggle");
});
/* Button Yakin Remove Kolom Group Header */
$("body").on('click', '#btn-yakin-remove-set-skpd', function(event) {
	event.preventDefault();
	/* Act on the event */
	var tobe_removed = $("input[name='hapus_proses']").val();

	/* Delete Div */
	$(tobe_removed).remove();

	/* Toggle Modal */
	$("#modal-hapus-set-skpd").modal("toggle");
});

/* Button Lihat Data Master */
$("body").on('click', '.btn-lihat-data-master', function(event) {
	event.preventDefault();
	/* Act on the event */
	/* Get id */
	var id_data_master = $(this).data("id-data-master");
	var id_data_master_enc = $(this).data("id-data-master-enc");
	var tahun_data = $(this).data("tahun-data");

	/* Window Open */
	newwindow = window.open(BASE_URL + GOODER_PATH.data_master_view_api + API_EDATA + "/" + id_data_master + "/" + tahun_data, "data-master-" + id_data_master + "-" + tahun_data, "height=650,width=1200");
	if (window.focus) { newwindow.focus() };
});

/* Button Upload Data Master */
$("body").on('click', '.btn-upload-data-master', function(event) {
	event.preventDefault();
	/* Act on the event */
	/* Get id */
	var id_data_master = $(this).data("id-data-master");
	var id_data_master_enc = $(this).data("id-data-master-enc");
	var tahun_data = $(this).data("tahun-data");

	/* Window Open */
	newwindow = window.open(BASE_URL + GOODER_PATH.data_master_upload_api + API_EDATA + "/" + id_data_master + "/" + tahun_data, "data-master-" + id_data_master + "-" + tahun_data, "height=650,width=1200");
	if (window.focus) { newwindow.focus() };
});

/* Button Download Format Excel Data Master */
$("body").on('click', '.btn-download-format-data-master', function(event) {
	event.preventDefault();
	/* Act on the event */
	var id_data_master = $(this).data("id-data-master");

	location.href = BASE_URL + GOODER_PATH.data_master_download_popup + "format/" + id_data_master;
});

/* Button Download File Excel Data Master */
$("body").on('click', '.btn-download-excel-data-master', function(event) {
	event.preventDefault();
	/* Act on the event */
	var id_data_master = $(this).data("id-data-master");
	var tahun_data = $(this).data("tahun-data");

	location.href = BASE_URL + GOODER_PATH.data_master_download_popup + "file-excel/" + id_data_master + "/" + tahun_data;
});

/* Button Tambah Row Data Master */
$("body").on('click', '.btn-tambah-row-data-master', function(event) {
	event.preventDefault();
	/* Act on the event */
	/* Get id */
	var id_data_master_enc = $(this).data("id-data-master-enc");
	var tahun_data_enc = $(this).data("tahun-data-enc");

	/* Do the ajax */
	$.ajax({
		url: BASE_URL + GOODER_PATH.form_tambah_row_data,
		type: 'POST',
		data: {id_data_master_enc: id_data_master_enc, tahun_data_enc: tahun_data_enc},
		dataType: 'html',
		success: function(result){
			/* Masukkan Form hasil loading */
			$("#body-tambah-row-data-master").html(result);

			/* Show Modal Tambah */
			$("#modal-tambah-row-data-master").modal("toggle");
		}
	});
});

/* Button Edit Row Data Master */
$("body").on('click', '.btn-edit-row-data-master', function(event) {
	event.preventDefault();
	/* Act on the event */
	/* Get id */
	var id_data_enc = $(this).data("id-data-enc");

	/* Do the ajax */
	$.ajax({
		url: BASE_URL + GOODER_PATH.form_edit_row_data,
		type: 'POST',
		data: {id_data_enc: id_data_enc},
		dataType: 'html',
		success: function(result){
			/* Masukkan Form hasil loading */
			$("#body-edit-row-data-master").html(result);

			/* Show Modal Tambah */
			$("#modal-edit-row-data-master").modal("toggle");
		}
	});
});

/* Button Hapus Row Data Master */
$("body").on('click', '.btn-hapus-row-data-master', function(event) {
	event.preventDefault();
	/* Act on the event */
	/* Get id */
	var id_data_enc = $(this).data("id-data-enc");

	/* Do the ajax */
	$.ajax({
		url: BASE_URL + GOODER_PATH.form_hapus_row_data,
		type: 'POST',
		data: {id_data_enc: id_data_enc},
		dataType: 'html',
		success: function(result){
			/* Masukkan Form hasil loading */
			$("#body-hapus-row-data-master").html(result);

			/* Show Modal Tambah */
			$("#modal-hapus-row-data-master").modal("toggle");
		}
	});
});
/* Button Refresh Page */
$("body").on('click', '.btn-refresh-page', function(event) {
	event.preventDefault();
	/* Act on the event */
	location.reload();
});

/* END DATA MASTER */

/* Button RAWDATA */
$("body").on('click', '#btn-tambah-set-realisasi', function(event) {
	event.preventDefault();
	/* Act on the event */
	/* Get id */
	var id_rawdata = $(this).data('id-rawdata');

	/* Do the ajax */
	$.ajax({
		url: BASE_URL + GOODER_PATH.tambah_set_realisasi + id_rawdata,
		type: 'POST',
		dataType: 'html',
		beforeSend: function(){
			/* Show Loading */
			$("#loading-tambah-set-realisasi").show();
		},
		success: function(result){
			/* Tambah Kolom */
			$("#kolom-set-realisasi").append(result);

			/* Hide Loading */
			$("#loading-tambah-set-realisasi").hide();
		}
	});
});
/* Button Remove Set Realisasi */
$("body").on('click', '.btn-remove-set-realisasi', function(event) {
	event.preventDefault();
	/* Act on the event */
	/* Get to be removed */
	var tobe_hidden = $(this).data('tobe-hidden');
	var tobe_showed = $(this).data('tobe-showed');
	var index_removed = $(this).data('index-removed');

	/* Show and hide */
	$(tobe_hidden).hide();
	$(tobe_showed).show();

	/* Set input hidden */
	$("#input-deleted-" + index_removed).val("yes");

	/* Toggle Modal */
	// $("#modal-hapus-set-realisasi").modal("toggle");
});
/* Undo Set Realisasi */
$("body").on('click', '.btn-undo-set-realisasi', function(event) {
	event.preventDefault();
	/* Act on the event */
	/* Get to be removed */
	var tobe_hidden = $(this).data('tobe-hidden');
	var tobe_showed = $(this).data('tobe-showed');
	var index_undo = $(this).data('index-undo');

	/* Show and hide */
	$(tobe_hidden).hide();
	$(tobe_showed).show();

	/* Set input hidden */
	$("#input-deleted-" + index_undo).val("no");
});
/* Button Get Realisasi Triwulan */
$("body").on('click', '.btn-get-rawdata-realisasi', function(event) {
	event.preventDefault();
	/* Act on the event */
	var id_realisasi = $(this).data("id-realisasi");

	/* Show Notification */
	show_notification("Memasukkan rawdata realisasi ke dalam antrian. Mohon Tunggu", "information");

	/* Do the ajax */
	$.ajax({
		url: BASE_URL + GOODER_PATH.set_rawdata_realisasi,
		type: 'POST',
		data: {id_realisasi: id_realisasi},
		dataType: 'html',
		beforeSend: function(){
			/* Show Loading */
			$("#icon-get-rawdata-realisasi-" + id_realisasi).hide();
			$("#loading-get-rawdata-realisasi-" + id_realisasi).show();
		},
		success: function(result_json){
			/* Parse JSON */
			var result =  $.parseJSON(result_json);

			/* Show Loading and hide icon */
			$("#icon-get-rawdata-realisasi-" + id_realisasi).show();
			$("#loading-get-rawdata-realisasi-" + id_realisasi).hide();

			/* Notification */
			show_notification(result.messages, result.status);
		}
	});

	// setTimeout(function(){ alert(sub_id+" : "+triwulan_ke); $("#row-get-realisasi-triwulan-"+sub_id+"-"+triwulan_ke).removeProp('disabled'); }, 1000 );

});
/* END RAWDATA */

/* BUTTON INDIKATOR */
/* Set Relasi */
$("body").on('click', '#btn-tambah-relasi-indikator', function(event) {
	event.preventDefault();
	/* Act on the event */
	/* Get id */
	var kode_indikator = $(this).data('kode-indikator');

	/* Do the ajax */
	$.ajax({
		url: BASE_URL + GOODER_PATH.tambah_relasi_indikator + kode_indikator,
		type: 'POST',
		dataType: 'html',
		beforeSend: function(){
			/* Show Loading */
			$("#loading-tambah-relasi-indikator").show();
		},
		success: function(result){
			/* Tambah Kolom */
			$("#kolom-relasi-indikator").append(result);

			/* Hide Loading */
			$("#loading-tambah-relasi-indikator").hide();
		}
	});
});
/* Remove Set Relasi Indikator */
$("body").on('click', '.btn-remove-relasi-indikator', function(event) {
	event.preventDefault();
	/* Act on the event */
	/* Get to be removed */
	var tobe_hidden = $(this).data('tobe-hidden');
	var tobe_showed = $(this).data('tobe-showed');
	var index_removed = $(this).data('index-removed');

	/* Show and hide */
	$(tobe_hidden).hide();
	$(tobe_showed).show();

	/* Set input hidden */
	$("#input-deleted-" + index_removed).val("yes");
});
/* Undo Set Relasi Indikator */
$("body").on('click', '.btn-undo-relasi-indikator', function(event) {
	event.preventDefault();
	/* Act on the event */
	/* Get to be removed */
	var tobe_hidden = $(this).data('tobe-hidden');
	var tobe_showed = $(this).data('tobe-showed');
	var index_undo = $(this).data('index-undo');

	/* Show and hide */
	$(tobe_hidden).hide();
	$(tobe_showed).show();

	/* Set input hidden */
	$("#input-deleted-" + index_undo).val("no");
});
/* END INDIKATOR */

/* Window Open */
// newwindow = window.open(BASE_URL + GOODER_PATH.data_master_view_api + "/" + id_data_master, "data-master-" + id_data_master, "height=650,width=1200");
// if (window.focus) { newwindow.focus() };


/* BUTTON CREATE DATA MASTER */
$("body").on('click', '.btn-simpan-create-data-master', function(event) {
	event.preventDefault();
	/* Act on the event */
	$("#form-create-data-master").ajaxSubmit({
		beforeSubmit: function(){
			/* Do Something */
			$("#panel-body-form").hide();
			$("#panel-proses-create-data-master").show();
		},
		success: function(result_json){
			/* Do Something */
			$("#panel-proses-create-data-master").hide();
			$("#panel-done-create-data-master").show();
		}
	});
	
});

/* END CREATE DATA MASTER */


/* BUTTON GRAFIK */
$("body").on('click', '.btn-tambah-grafik', function(event) {
	event.preventDefault();
	/* Act on the event */
	/* Get id */
	var data_atom_id_enc = $("#select-data-atom").val();

	/* Do the ajax */
	$.ajax({
		url: BASE_URL + GOODER_PATH.tambah_data_grafik + data_atom_id_enc,
		type: 'POST',
		dataType: 'html',
		beforeSend: function(){
			/* Show Loading */
			$(".loading-refresh-grafik").show();
			$(".hasil-refresh-grafik").hide();
		},
		success: function(result_json){
			/* Hide Loading */
			$(".loading-refresh-grafik").hide();
			$(".hasil-refresh-grafik").show();

			/* Parse JSON */
			var result =  $.parseJSON(result_json);

			/* Check result */
			if (result.status == "error") {
				/* Notification */
				show_notification(result.messages, result.status);
			} else {
				/* Set Jumlah Grafik */
				if (BAR_LIST.length == 0) {
					set_new_grafik(result.data.to_load);
					BAR_LIST.push(result.data.detail.data_atom_narasi);
				} else {
					if (BAR_LIST.indexOf(result.data.detail.data_atom_narasi) != -1) {
						toggle_grafik(result.data.to_load);
					} else {
						BAR_LIST.push(result.data.detail.data_atom_narasi);
						load_grafik(result.data.to_load);
					}
				}
				
				/* Tambah di List */
				$("#body-list-data-atom").append('<div class="col-lg-3" id="button-remove-grafik-' + result.data.detail.data_atom_id + '"><button type="button" data-popup="tooltip" data-placement="top" title="Hapus dari grafik" class="btn btn-danger btn-remove-grafik" data-to-remove="#button-remove-grafik-' + result.data.detail.data_atom_id + '" data-to-remove-narasi="' + result.data.detail.data_atom_narasi + '"><i class="icon-trash position-left"></i>' + result.data.detail.data_atom_narasi + '</button></div>');
				$('[data-popup="tooltip"]').tooltip();

				/* Notification */
				show_notification(result.messages, result.status);
			}
			
		}
	});
	
});

$("body").on('click', '.btn-remove-grafik', function(event) {
	event.preventDefault();
	/* Act on the event */
	/* Get data */
	var button_removed = $(this).data("to-remove");
	var data_atom_narasi = $(this).data("to-remove-narasi");

	/* Hapus dari grafik */
	toggle_grafik(data_atom_narasi);

	/* Hidden button */
	$(button_removed).remove();

	/* Notification */
	show_notification('Berhasil hapus ' + data_atom_narasi + ' dari grafik', "success");
	
});
/* END BUTTON GRAFIK */

/* BUTTON DATA ATOM */
/* Button Hapus Data Atom */
$("body").on('click', '.btn-hapus-data-atom', function(event) {
	event.preventDefault();
	/* Act on the event */
	var data_atom_id = $(this).data("id-data-atom");

	/* Set input */
	$(".input-id-data-atom").val(data_atom_id);

	/* Toggle Modal */
	$("#modal-hapus-data-atom").modal("toggle");
});
/* END BUTTON DATA ATOM */