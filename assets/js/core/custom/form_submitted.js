/* ------------------------------------------------------------------------------
*
*  # All Form Submit JS
*
*  JS for All Form Submitted in System
*
*  Version: 1.0
*  Latest update: Jul 31, 2017
*
* ---------------------------------------------------------------------------- */
/* START CODE */
/* VARIABEL DECLARATION */

/* FUNCTION DECLARATION */
function refresh_filter(to_refresh) {
	$(".form-" + to_refresh).ajaxSubmit({
		beforeSubmit: function(){
			/* Do Something */
			/* Hide Hasil */
			$(".hasil-" + to_refresh).hide();

			/* Show Loading */
			$(".loading-" + to_refresh).show();
		},
		success: function(result){
			/* Hide Loading */
			$(".loading-" + to_refresh).hide();

			/* Show Hasil */
			$(".hasil-" + to_refresh).html(result);
			$(".hasil-" + to_refresh).show();
		}
	});
}

/* ACTION DECLARATION */
/* Refresh Filter */
$("body").on('click', '.btn-refresh-filter', function(event) {
	event.preventDefault();
	/* Act on the event */
	/* Get data form */
	var to_refresh = $(this).data("to-refresh");
	/* Submit Form */
	refresh_filter(to_refresh);
});
/* Hapus User Submitted */
$("body").on('click', '#btn-proses-hapus-user', function(event) {
	event.preventDefault();
	/* Act on the event */
	/* Submit Form */
	$("#form-hapus-user").ajaxSubmit({
		beforeSubmit: function(){
			/* Do Something */
		},
		success: function(result_json){
			/* Parse JSON */
			var result =  $.parseJSON(result_json);

			/* Check status */
			if (result.status == "success") {
				/* Hide Row */
				$("#row-user-"+result.user_id_deleted).hide();

				/* Show status */
				show_notification(result.messages, "success");

				/* Modal Close */
				$("#modal-hapus-user").modal("toggle");

				/* Reset Form */
				$("#form-hapus-user").resetForm();
			} else {
				/* Show status */
				show_notification(result.messages, "error");

				/* Modal Close */
				$("#modal-hapus-user").modal("toggle");
			}
		}
	});
});

/* Unlock User Submitted */
$("body").on('click', '#btn-proses-unlock-user', function(event) {
	event.preventDefault();
	/* Act on the event */
	/* Submit Form */
	$("#form-unlock-user").ajaxSubmit({
		beforeSubmit: function(){
			/* Do Something */
		},
		success: function(result_json){
			/* Parse JSON */
			var result =  $.parseJSON(result_json);

			/* Check status */
			if (result.status == "success") {
				/* Hide and Show Button */
				$(".status-unlock-user-"+result.user_id).hide();
				$(".status-lock-user-"+result.user_id).show();

				/* Show status */
				show_notification(result.messages, "success");

				/* Modal Close */
				$("#modal-unlock-user").modal("toggle");

				/* Reset Form */
				$("#form-unlock-user").resetForm();
			} else {
				/* Show status */
				show_notification(result.messages, "error");

				/* Modal Close */
				$("#modal-unlock-user").modal("toggle");
			}
		}
	});
});

/* Lock User Submitted */
$("body").on('click', '#btn-proses-lock-user', function(event) {
	event.preventDefault();
	/* Act on the event */
	/* Submit Form */
	$("#form-lock-user").ajaxSubmit({
		beforeSubmit: function(){
			/* Do Something */
		},
		success: function(result_json){
			/* Parse JSON */
			var result =  $.parseJSON(result_json);

			/* Check status */
			if (result.status == "success") {
				/* Hide and Show Button */
				$(".status-lock-user-"+result.user_id).hide();
				$(".status-unlock-user-"+result.user_id).show();

				/* Show status */
				show_notification(result.messages, "success");

				/* Modal Close */
				$("#modal-lock-user").modal("toggle");

				/* Reset Form */
				$("#form-lock-user").resetForm();
			} else {
				/* Show status */
				show_notification(result.messages, "error");

				/* Modal Close */
				$("#modal-lock-user").modal("toggle");
			}
		}
	});
});

/* Filter Unit Kerja */
$("body").on('change', '#select-filter-unit-kerja', function(event) {
	event.preventDefault();
	/* Act on the event */
	/* Submit Form */
	$("#form-filter-unit-kerja").ajaxSubmit({
		beforeSubmit: function(){
			/* Do Something */
			/* Hide Hasil */
			$("#hasil-filter-unit-kerja").hide();

			/* Show Loading */
			$("#loading-filter-unit-kerja").show();
		},
		success: function(result){
			/* Hide Loading */
			$("#loading-filter-unit-kerja").hide();

			/* Show Hasil */
			$("#hasil-filter-unit-kerja").html(result);
			$("#hasil-filter-unit-kerja").show();
		}
	});
});

/* Filter Group Header */
$("body").on('change', '#select-filter-group-header', function(event) {
	event.preventDefault();
	/* Act on the event */
	/* Submit Form */
	$("#form-filter-group-header").ajaxSubmit({
		beforeSubmit: function(){
			/* Do Something */
			/* Hide Hasil */
			$("#hasil-filter-group-header").hide();

			/* Show Loading */
			$("#loading-filter-group-header").show();
		},
		success: function(result){
			/* Hide Loading */
			$("#loading-filter-group-header").hide();

			/* Show Hasil */
			$("#hasil-filter-group-header").html(result);
			$("#hasil-filter-group-header").show();
		}
	});
});

/* Filter Urusan */
$("body").on('change', '#select-filter-urusan', function(event) {
	event.preventDefault();
	/* Act on the event */
	/* Submit Form */
	$("#form-filter-urusan").ajaxSubmit({
		beforeSubmit: function(){
			/* Do Something */
			/* Hide Hasil */
			$("#hasil-filter-urusan").hide();

			/* Show Loading */
			$("#loading-filter-urusan").show();
		},
		success: function(result){
			/* Hide Loading */
			$("#loading-filter-urusan").hide();

			/* Show Hasil */
			$("#hasil-filter-urusan").html(result);
			$("#hasil-filter-urusan").show();
		}
	});
});

/* Filter Triwulan */
$("body").on('change', '#select-filter-triwulan', function(event) {
	event.preventDefault();
	/* Act on the event */
	/* Submit Form */
	refresh_filter("filter-triwulan");
});
/* Filter Semester */
$("body").on('change', '#select-filter-semester', function(event) {
	event.preventDefault();
	/* Act on the event */
	/* Submit Form */
	refresh_filter("filter-semester");
});

/* Upload File */
$("body").on('click', '.btn-proses-upload', function(event) {
	event.preventDefault();
	/* Act on the event */
	var jenis_file = $(this).data("jenis-file");

	/* Submit Form */
	$("#form-upload-" + jenis_file).ajaxSubmit({
		beforeSubmit: function(){
			/* Do Something */
			/* Hide body, Show loading */
			$("#body-upload-" + jenis_file).hide();
			$("#loading-upload-" + jenis_file).show();
			$("#persen-upload-" + jenis_file).html("Tunggu Sebentar");
		},
		uploadProgress: function(ev, pos, tot, persen){
			$("#persen-upload-" + jenis_file).html(persen + "%");
		},
		success: function(result_json){
			/* Parse JSON */
			var result =  $.parseJSON(result_json);

			/* Check status */
			if (result.status == "success") {
				/* Set Pesan sebelum berakhir */
				$("#messages-upload-selesai").html(result.messages);
				$("#panel-upload-" + jenis_file).hide();
				$("#panel-upload-selesai").show();

				if(jenis_file == "file-pendukung"){
					/* file pendukung */
					setTimeout(function(){ location.reload(); }, 2000 );
				} else {
					/* Selain file pendukung */
					setTimeout(function(){ self.close(); }, 2000 );
				}

			} else {
				/* Reset Form */
				$("#form-upload-" + jenis_file).resetForm();

				/* Show Messages */
				show_notification(result.messages, "error");

				/* show body, hide loading */
				$("#loading-upload-" + jenis_file).hide();
				$("#body-upload-" + jenis_file).show();
			}
		}
	});
});

/* Hapus Data Master */
$("body").on('click', '#btn-proses-hapus-data-master', function(event) {
	event.preventDefault();
	/* Act on the event */
	/* Submit Form */
	$("#form-hapus-data-master").ajaxSubmit({
		beforeSubmit: function(){
			/* Do Something */
		},
		success: function(result_json){
			/* Parse JSON */
			var result =  $.parseJSON(result_json);

			/* Check status */
			if (result.status == "success") {
				/* Hide Row */
				$("#row-data-master-"+result.data_master_id_deleted).hide();

				/* Show status */
				show_notification(result.messages, "success");

				/* Modal Close */
				$("#modal-hapus-data-master").modal("toggle");

				/* Reset Form */
				$("#form-hapus-data-master").resetForm();
			} else {
				/* Show status */
				show_notification(result.messages, "error");

				/* Modal Close */
				$("#modal-hapus-data-master").modal("toggle");
			}
		}
	});
});

/* Button Proses tambah row data master */
$("body").on('click', '#btn-proses-tambah-row-data-master', function(event) {
	event.preventDefault();
	/* Act on the event */
	/* Submit Form */
	$("#form-tambah-row-data").ajaxSubmit({
		beforeSubmit: function(){
			/* Do Something */
			$("#loading-tambah-row-data-master").show();
			$("#body-form-tambah-row-data").hide();
		},
		success: function(result_json){
			/* Parse JSON */
			var result =  $.parseJSON(result_json);

			/* Check status */
			if (result.status == "success") {
				/* Show Refresh Button */
				$(".btn-refresh-page").show();
				$(".btn-tambah-row-data-master").hide();

				/* Show status */
				show_notification(result.messages, "success");

				/* Modal Close */
				$("#modal-tambah-row-data-master").modal("toggle");
			} else {
				/* Show status */
				show_notification(result.messages, "error");

				/* Modal Close */
				$("#modal-tambah-row-data-master").modal("toggle");
			}
		}
	});

});

/* Button Proses edit row data master */
$("body").on('click', '#btn-proses-edit-row-data-master', function(event) {
	event.preventDefault();
	/* Act on the event */
	/* Submit Form */
	$("#form-edit-row-data").ajaxSubmit({
		beforeSubmit: function(){
			/* Do Something */
			$("#loading-edit-row-data-master").show();
			$("#body-form-edit-row-data").hide();
		},
		success: function(result_json){
			/* Parse JSON */
			var result =  $.parseJSON(result_json);

			/* Check status */
			if (result.status == "success") {
				/* Show Refresh Button */
				$(".btn-refresh-page").show();
				$(".btn-tambah-row-data-master").hide();

				/* Show status */
				show_notification(result.messages, "success");

				/* Modal Close */
				$("#modal-edit-row-data-master").modal("toggle");
			} else {
				/* Show status */
				show_notification(result.messages, "error");

				/* Modal Close */
				$("#modal-edit-row-data-master").modal("toggle");
			}
		}
	});

});

/* Button Proses hapus row data master */
$("body").on('click', '#btn-proses-hapus-row-data-master', function(event) {
	event.preventDefault();
	/* Act on the event */
	/* Submit Form */
	$("#form-hapus-row-data").ajaxSubmit({
		beforeSubmit: function(){
			/* Do Something */
			$("#loading-hapus-row-data-master").show();
			$("#body-form-hapus-row-data").hide();
		},
		success: function(result_json){
			/* Parse JSON */
			var result =  $.parseJSON(result_json);

			/* Check status */
			if (result.status == "success") {
				/* Show Refresh Button */
				$(".btn-refresh-page").show();
				$(".btn-tambah-row-data-master").hide();

				/* Show status */
				show_notification(result.messages, "success");

				/* Modal Close */
				$("#modal-hapus-row-data-master").modal("toggle");
			} else {
				/* Show status */
				show_notification(result.messages, "error");

				/* Modal Close */
				$("#modal-hapus-row-data-master").modal("toggle");
			}
		}
	});

});

/* Change Select Kolom Realisasi Data Master */
$("body").on('change', '.select-kolom-ke-set-realisasi', function(event) {
	event.preventDefault();
	/* Act on the event */

	var index_realisasi = $(this).data("index-realisasi");
	var filter_ke = $(this).data("filter-ke");

	/* Get selected value */
	var selected_val = $(this).find(":selected").val();

	// alert(index_realisasi + " : " + filter_ke + " : " + selected_val);
	/* Kirim ke controller */
	// $("#karakter-filter-" + index_realisasi + "-" + filter_ke).html(index_realisasi + " : " + filter_ke + " : " + selected_val);
	$.ajax({
		url: BASE_URL + GOODER_PATH.karakter_filter,
		type: 'POST',
		data: {index_realisasi: index_realisasi, filter_ke: filter_ke, kolom_ke: selected_val},
		dataType: 'html',
		beforeSend: function(){
			/* Show Loading */
			$("#loading-proses-karakter-filter-" + index_realisasi + "-" + filter_ke).show();
			$("#karakter-filter-" + index_realisasi + "-" + filter_ke).hide();
		},
		success: function(result){
			/* Tambah Kolom */
			$("#karakter-filter-" + index_realisasi + "-" + filter_ke).html(result);

			/* Hide Loading */
			$("#loading-proses-karakter-filter-" + index_realisasi + "-" + filter_ke).hide();
			$("#karakter-filter-" + index_realisasi + "-" + filter_ke).show();
		}
	});

});

$("body").on('click', '#btn-proses-hapus-data-atom', function(event) {
	event.preventDefault();
	/* Act on the event */
	/* Submit Form */
	$("#form-hapus-data-atom").ajaxSubmit({
		beforeSubmit: function(){
			/* Do Something */
		},
		success: function(result_json){
			/* Parse JSON */
			var result =  $.parseJSON(result_json);

			/* Check status */
			if (result.status == "success") {
				/* Hide Row */
				$("#row-data-atom-"+result.data_atom_id_deleted).hide();

				/* Show status */
				show_notification(result.messages, result.status);

				/* Modal Close */
				$("#modal-hapus-data-atom").modal("toggle");

				/* Reset Form */
				$("#form-hapus-data-atom").resetForm();
			} else {
				/* Show status */
				show_notification(result.messages, result.status);

				/* Modal Close */
				$("#modal-hapus-data-atom").modal("toggle");
			}
		}
	});
});