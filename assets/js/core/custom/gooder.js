/* ------------------------------------------------------------------------------
*
*  # Gooder JS
*
*  Gooder js for another functionality
*
*  Version: 1.0
*  Latest update: Jul 31, 2017
*
* ---------------------------------------------------------------------------- */
/* START CODE */
/* VARIABEL DECLARATION */
var GOODER_PATH = {
	"user_aktifasi" : "dashboard/manajemen/user/aktivasi/",

	/* SET */
	"set_rawdata_realisasi" : "dashboard/realisasi/data-atom/realisasi-ulang/",

	/* DOM */
	"search_ref" : "dashboard/manajemen/member/search-ref/",
	"tambah_kolom_group" : "dashboard/master/group-header/tambah-kolom/",
	"tambah_kolom_data" : "dashboard/setting/data-master/tambah-kolom/",
	"tambah_set_skpd" : "dashboard/setting/data-master/tambah-set-skpd/",
	"tambah_set_realisasi" : "dashboard/setting/data-atom/tambah-set-realisasi/",
	"tambah_relasi_indikator" : "dashboard/setting/relasi-indikator/tambah-relasi/",
	"karakter_filter" : "dashboard/setting/data-atom/karakter-filter/",

	/* API */
	"data_master_view_api" : "api/data-master/view-data/",
	"data_master_upload_api" : "api/data-master/upload-data/",

	/* POPUP */
	"data_master_download_popup" : "popup/download/data-master/",
	"form_tambah_row_data" : "popup/review/data-master/form-tambah-row-data/",
	"form_edit_row_data" : "popup/review/data-master/form-edit-row-data/",
	"form_hapus_row_data" : "popup/review/data-master/form-hapus-row-data/",

	/* DATA */
	"tambah_data_grafik" : "dashboard/report/data-atom/tambah-grafik/",
	"get_kolom_data_master" : "dashboard/setting/data-atom/get-kolom-data-master/",
};

var API_EDATA = "";

var TEMP_UPLOAD = [];

var URUTAN_KOLOM_DATA = "";

var BAR_CHART;
var BAR_LIST = [];


/* FUNCTION DECLARATION */
function show_notification(text, type){
	$(function() {
		noty({
		    width: 200,
		    text: text,
		    type: type,
		    dismissQueue: true,
		    timeout: 2000,
		    layout: 'top'
		});
	});
};

function rubah_urutan_kolom() {
	urutane = "";

	/* Mulai Proses Check Urutan */
	var fieldset = $("#kolom-data > .panel-group");
	$.each(fieldset, function(key, value){
		urutane += $(value).data("id-kolom")+";";
	});
	URUTAN_KOLOM_DATA = urutane;

	/* Masukkan ke FORM */
	$("input[name='urutan_kolom_key']").val(URUTAN_KOLOM_DATA);
}

$("body").on('change', '.radio-group-header', function(event) {
	event.preventDefault();
	/* Act on the event */

	var kolom_ke = $(this).val();

	/* Hide lainnya */
	$(".realisasi-group-header").hide();

	/* Show Kolom ke */
	$("#realisasi-group-header-" + kolom_ke).show();
});

$("body").on('change', '#select-data-atom-data-master', function(event) {
	event.preventDefault();
	/* Act on the event */

	var data_master_id = $(this).val();

	/* Do the ajax */
	$.ajax({
		url: BASE_URL + GOODER_PATH.get_kolom_data_master,
		type: 'POST',
		data: {data_master_id: data_master_id},
		dataType: 'html',
		beforeSend: function(){
			$("#loading-proses").show();
		},
		success: function(result){
			$("#loading-proses").hide();
			$("#body-set-realisasi").html(result);
		}
	});
});

/* Function set Grafik baru */
function set_new_grafik(data_to_load) {
	BAR_CHART = c3.generate({
        bindto: '#c3-bar-chart',
        size: { height: 400 },
        data: {
            columns: [data_to_load],
            type: 'bar'
        },
        axis: {
			x: {
				type: 'category',
				categories: TAHUN_DATA
			}
		},
        bar: {
            width: {
                ratio: 0.5
            }
        },
        grid: {
            y: {
                show: true
            }
        }
    });
}
/* Load Grafik tambah */
// function load_grafik(data_to_load, color) {
function load_grafik(data_to_load) {
	BAR_CHART.load({
        columns: [
            data_to_load
        ],
        // color: {
        //     pattern: [color]
        // },
    });
}
/* Hapus dari grafik */
function hapus_grafik(data_to_remove) {
	BAR_CHART.unload({
    	ids:[data_to_remove]
	});
}
function toggle_grafik(data_to) {
	BAR_CHART.toggle(data_to);
}
/* destroy grafik */
function destroy_grafik() {
	BAR_CHART = BAR_CHART.destroy();
}
/* flush grafik */
function flush_grafik() {
	BAR_CHART.flush();
}

// $("body").on('change', '.checkbox-rawdata-angka', function(event) {
// 	event.preventDefault();
// 	/* Act on the event */
//
// 	/* Get is checked */
// 	var checked = $(this).is("checked");
// 	alert(checked);
//
// 	/* Show or hide option */
// 	if (checked) {
// 		$("#if-not-angka").hide();
// 		alert("berhasil checked");
// 	} else {
// 		$("#if-not-angka").show();
// 		alert("berhasil unchecked");
// 	}
// });

/* Dragula */
dragula([document.getElementById('kolom-data'), document.getElementById('kolom-data-right')], {
	removeOnSpill: false
}).on('drop', function (el) {
	rubah_urutan_kolom();
});

/* Date Picker */
/*$('.datepicker').datepicker({
    format:'yyyy-mm-dd',
    todayHighlight:true,
    daysOfWeekHighlighted:[0,6]
});*/


// dragula([document.getElementById('kolom-data'), document.getElementById('kolom-data-right')], {removeOnSpill: false})
// .on('drag', function (el) {
// 	el.className = el.className.replace(' ex-moved', '');
// }).on('drop', function (el) {
// 	el.className += ' ex-moved';
// 	rubah_urutan_kolom();

// }).on('over', function (el, container) {
// 	container.className += ' ex-over';
// }).on('out', function (el, container) {
// 	container.className = container.className.replace(' ex-over', '');
// });


// dragula([document.getElementById(left), document.getElementById(right)], {
// 	copy: function (el, source) {
// 		return source === document.getElementById(left)
// 	},
// 	accepts: function (el, target) {
// 		return target !== document.getElementById(left)
// 	}
// });


/* END CODE */
