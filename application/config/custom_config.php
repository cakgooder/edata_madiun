<?php

/* CONFIG FOR SYSTEM */
$config['tag_apps'] = 'EDATA';
$config['tag_apps_brand'] = 'EDATA';
$config['tag_apps_system'] = 'Edata';
$config['tag_meta_desc'] = 'Sistem Management Data oleh Dinas Komunikasi dan Informatika Kota Madiun';

/* CONFIG FOR COMPANY */
$config['company_copyright'] = 'Dinas Komunikasi dan Informatika';
$config['company_email'] = 'dirga.gooder@gmail.com';
$config['company_city'] = 'Kota Madiun';
$config['company_site'] = 'https://madiunkota.go.id/';

/* Kode Tipe data untuk keperluan masuk ke data */
$config['type_data']["int"] = "Integer";
$config['type_data']["bool"] = "Boolean";
$config['type_data']["char"] = "Character";
$config['type_data']["dec"] = "Decimal";
$config['type_data']["date"] = "Date";

/* Kode jenis Realisasi data */
$config['type_realisasi']["COUNTALL"] = "COUNT ALL (Hitung semua data)";
$config['type_realisasi']["COUNTDIS"] = "COUNT DISTINCT (Hitung tanpa berulang)";
$config['type_realisasi']["COUNTDUP"] = "COUNT DUPLIKASI (Hitung Jumlah Duplikasi)";
$config['type_realisasi']["SUM"] = "SUM (Total)";
$config['type_realisasi']["AVG"] = "AVERAGE (Rata-rata)";
$config['type_realisasi']["MAX"] = "MAX (Nilai Tertinggi)";
$config['type_realisasi']["MIN"] = "MIN (Nilai Terendah)";

/* Kode Operator */
$config["type_operator"]["<"] = "Kurang Dari ( < )";
$config["type_operator"][">"] = "Lebih Dari ( > )";
$config["type_operator"]["="] = "Sama Dengan ( = )";
$config["type_operator"]["!="] = "Tidak Sama Dengan ( != )";
$config["type_operator"]["like"] = "Mendekati String ( LIKE )";
$config["type_operator"]["<="] = "Kurang Dari Sama Dengan ( <= )";
$config["type_operator"][">="] = "Lebih Dari Sama Dengan ( >= )";
$config["type_operator"]["is_not_null"] = "Tidak Kosong ( is not null )";
$config["type_operator"]["is_null"] = "Kosong ( is null )";