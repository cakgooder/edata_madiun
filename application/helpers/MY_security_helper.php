<?php
/* Fungsi untuk enkripsi kode */
function encrypting_code($text_to_be)
{
	/* Get instance CI */
	$CI =& get_instance();

	/* Encryption Initialisation (keperluan keamanan) */
	$CI->encryption->initialize(
        array(
            'cipher' => 'blowfish',
            'mode' => 'cfb',
        )
	);

	/* Return the encrypt text */
	return base64_encode($CI->encryption->encrypt($text_to_be));
}
function decrypting_code($text_to_be)
{
	/* Get instance CI */
	$CI =& get_instance();

	/* Encryption Initialisation (keperluan keamanan) */
	$CI->encryption->initialize(
        array(
            'cipher' => 'blowfish',
            'mode' => 'cfb',
        )
	);

	/* Return the encrypt text */
	return $CI->encryption->decrypt(base64_decode($text_to_be));
}