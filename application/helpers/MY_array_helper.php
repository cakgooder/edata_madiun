<?php

/* Function to convert array to string */
function convert_array_to_string($data_array = "", $splitter = ";")
{
	/* Prepare variable */
	$string_array = "";

	/* Check Array */
	if (is_array($data_array)) {
		/* Loop Data array */
		foreach ($data_array as $data) {
			$string_array .= $data.$splitter;
		}
	}

	/* Remove last character */
	$string_array = rtrim($string_array, $splitter);

	/* Return string */
	return $string_array;
}

/* Function to convert string to array */
function convert_string_to_array($string_array = "", $splitter = ";")
{
	/* Prepare variable */
	$data_array = [];

	/* Check String */
	if (!empty($string_array)) {
		/* Split by splitter */
		$data_array = explode($splitter, $string_array);
	}

	/* Return array */
	return $data_array;
}

/* Function to get config Upload XLS */
function get_config_upload_file($upload_path, $filename, $overwrite = FALSE, $allowed_types = 'xls|xlsx')
{
	$config['upload_path'] = $upload_path;
	$config['allowed_types'] = $allowed_types;
	$config['file_name'] = $filename;
	$config['max_size']     = '0';
	$config['overwrite']     = $overwrite;
	$config['remove_spaces']     = TRUE;
	$config['file_ext_tolower']     = TRUE;

	return $config;
}
