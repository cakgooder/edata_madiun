<?php
/* Function remove paragraf error upload */
function remove_paragraf_phrase($error_phrase = "")
{
	$error_phrase = str_replace("<p>", "", $error_phrase);
	$error_phrase = str_replace("</p>", "", $error_phrase);
	return $error_phrase;
}

/* Function remove space and change it with separator */
function clean_separate_word($sentence = "", $separator = "-")
{
	/* Clean sentence */
	$sentence = preg_replace('/[^A-Za-z0-9]/', ' ', $sentence);
	$sentence = preg_replace('/ +/', ' ', $sentence);

	/* Separate word */
	return preg_replace('/[\s]+/', $separator, trim(MB_ENABLED ? mb_strtolower($sentence) : strtolower($sentence)));
}

/* Function get list color */
function get_color_limitless($random = TRUE, $fullcolor = TRUE, $softness = NULL)
{
	/* List Color Basic */
	$list_color_basic = [
		/* Primary */
		"primary",
		/* Danger */
		"danger",
		/* Success */
		"success",
		/* Warning */
		"warning",
		/* Pink */
		"pink",
		/* Violet */
		"violet",
		/* Purple */
		"purple",
		/* Indigo */
		"indigo",
		/* Blue */
		"blue",
		/* Teal */
		"teal",
		/* Green */
		"green",
		/* Orange */
		"orange",
		/* Brown */
		"brown",
		/* Grey */
		"grey",
		/* Slate */
		"slate"
	];

	/* List Color Full */
	$list_color_full = [
		/* Primary */
		"primary", "primary-300", "primary-400", "primary-600", "primary-700", "primary-800",
		/* Danger */
		"danger", "danger-300", "danger-400", "danger-600", "danger-700", "danger-800",
		/* Success */
		"success", "success-300", "success-400", "success-600", "success-700", "success-800",
		/* Warning */
		"warning", "warning-300", "warning-400", "warning-600", "warning-700", "warning-800",
		/* Pink */
		"pink", "pink-300", "pink-400", "pink-600", "pink-700", "pink-800",
		/* Violet */
		"violet", "violet-300", "violet-400", "violet-600", "violet-700", "violet-800",
		/* Purple */
		"purple", "purple-300", "purple-400", "purple-600", "purple-700", "purple-800",
		/* Indigo */
		"indigo", "indigo-300", "indigo-400", "indigo-600", "indigo-700", "indigo-800",
		/* Blue */
		"blue", "blue-300", "blue-400", "blue-600", "blue-700", "blue-800",
		/* Teal */
		"teal", "teal-300", "teal-400", "teal-600", "teal-700", "teal-800",
		/* Green */
		"green", "green-300", "green-400", "green-600", "green-700", "green-800",
		/* Orange */
		"orange", "orange-300", "orange-400", "orange-600", "orange-700", "orange-800",
		/* Brown */
		"brown", "brown-300", "brown-400", "brown-600", "brown-700", "brown-800",
		/* Grey */
		"grey", "grey-300", "grey-400", "grey-600", "grey-700", "grey-800",
		/* Slate */
		"slate", "slate-300", "slate-400", "slate-600", "slate-700", "slate-800"
	];

	/* Check Random atau bukan */
	if($random){
		/* Random Angka */
		$random = rand(0, count($list_color)-1);

		/* Check Full Color atau Basic */
		if($fullcolor){
			return $list_color_full[$random];
		} else {
			/* Check softness */
			if (empty($softness)) {
				return $list_color_basic[$random];
			} else {
				return $list_color_basic[$random]."-".$softness;
			}
		}
	} else {
		/* Check Full Color atau Basic */
		if($fullcolor){
			return $list_color_full;
		} else {
			return $list_color_basic;
		}
	}

}

/* Get Color Berdasarkan Tahun */
function get_color_by_tahun($tahun)
{
	$color = "pink";
	switch ($tahun) {
		case '2014':
			$color = "grey";
			break;
		case '2015':
			$color = "brown";
			break;
		case '2016':
			$color = "pink";
			break;
		case '2017':
			$color = "primary";
			break;
		case '2018':
			$color = "success";
			break;
		case '2019':
			$color = "indigo";
			break;
		case '2020':
			$color = "violet";
			break;
		case '2021':
			$color = "orange";
			break;
		case '2022':
			$color = "grey";
			break;
		case '2023':
			$color = "brown";
			break;
		case '2024':
			$color = "pink";
			break;
		case '2025':
			$color = "primary";
			break;
		case '2026':
			$color = "success";
			break;
		case '2027':
			$color = "indigo";
			break;
		case '2028':
			$color = "violet";
			break;
		case '2029':
			$color = "orange";
			break;
		case '2030':
			$color = "grey";
			break;
		case '2031':
			$color = "brown";
			break;
		case '2032':
			$color = "pink";
			break;
		case '2033':
			$color = "primary";
			break;
		case '2034':
			$color = "success";
			break;
		case '2035':
			$color = "indigo";
			break;
		case '2036':
			$color = "violet";
			break;
		case '2037':
			$color = "orange";
			break;
		case '2038':
			$color = "orange";
			break;
		case '2039':
			$color = "grey";
			break;
		case '2040':
			$color = "brown";
			break;
		case '2041':
			$color = "pink";
			break;
		case '2042':
			$color = "primary";
			break;
		case '2043':
			$color = "success";
			break;
		case '2044':
			$color = "indigo";
			break;
		case '2045':
			$color = "violet";
			break;
		case '2046':
			$color = "orange";
			break;
		default:
			$color = "danger";
			break;
	}

	return $color;
}

/* Function to get bulan in Indonesia */
function get_bulan_indonesia($output_semua = TRUE, $bln_ke = 1, $jenis = "panjang")
{
	/* Set Bulan */
	$bulan = [
		1 => ["panjang" => "Januari", "pendek" => "Jan"],
		2 => ["panjang" => "Februari", "pendek" => "Feb"],
		3 => ["panjang" => "Maret", "pendek" => "Mar"],
		4 => ["panjang" => "April", "pendek" => "Apr"],
		5 => ["panjang" => "Mei", "pendek" => "Mei"],
		6 => ["panjang" => "Juni", "pendek" => "Jun"],
		7 => ["panjang" => "Juli", "pendek" => "Jul"],
		8 => ["panjang" => "Agustus", "pendek" => "Agu"],
		9 => ["panjang" => "September", "pendek" => "Sep"],
		10 => ["panjang" => "Oktober", "pendek" => "Okt"],
		11 => ["panjang" => "November", "pendek" => "Nov"],
		12 => ["panjang" => "Desember", "pendek" => "Des"]
	];

	/* Check Output */
	if ($output_semua) {
		return $bulan;
	} else {
		return $bulan[$bln_ke][$jenis];
	}
}

/* Function to get Triwulan */
function get_triwulan($tw_ke = 1, $jenis = TRUE)
{
	/* Check jenis */
	if ($jenis == TRUE) {
		$bulan = [];
		if ($tw_ke == 1) {
			$bulan = [1,2,3];
		} else if ($tw_ke == 2) {
			$bulan = [4,5,6];
		} else if ($tw_ke == 3) {
			$bulan = [7,8,9];
		} else if ($tw_ke == 4) {
			$bulan = [10,11,12];
		}
		return $bulan;
	} else {
		$bulan = "";
		if ($tw_ke == 1) {
			$bulan = "1,2,3";
		} else if ($tw_ke == 2) {
			$bulan = "4,5,6";
		} else if ($tw_ke == 3) {
			$bulan = "7,8,9";
		} else if ($tw_ke == 4) {
			$bulan = "10,11,12";
		}
		return $bulan;
	}

}

/* Function to get Semester */
function get_semester($smt_ke = 1, $jenis = TRUE)
{
	/* Check jenis */
	if ($jenis == TRUE) {
		$bulan = [];
		if ($smt_ke == 1) {
			$bulan = [1,2,3,4,5,6];
		} else if ($smt_ke == 2) {
			$bulan = [7,8,9,10,11,12];
		}
		return $bulan;
	} else {
		$bulan = "";
		if ($smt_ke == 1) {
			$bulan = "1, 2, 3, 4, 5, 6";
		} else if ($smt_ke == 2) {
			$bulan = "7, 8, 9, 10, 11, 12";
		}
		return $bulan;
	}

}

/* Function to get Bulan Sampai Dengan */
function get_bulan_sampai_dengan($sd = 1, $jenis = FALSE)
{
	/* Check bulan lebih dari 12 atau kurang dari 1 */
	if ($sd > 12) {
		$sd = 12;
	} else if($sd < 1){
		$sd = 1;
	}

	/* Check jenis */
	if ($jenis == TRUE) {
		$bulan = [];
		for ($i=1; $i <= $sd; $i++) {
			$bulan[] = $i;
		}

		return $bulan;
	} else {
		$bulan = "";
		for ($i=1; $i <= $sd ; $i++) {
			$bulan .= $i.", ";
		}

		return substr($bulan, 0, -2);
	}
}

/* Function to get current timestamp postgresql */
function current_timestamp_postgres()
{
	return date("Y-m-d H:i:s");
}
