<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	/* Construct */
	public function __construct()
	{
		parent::__construct();

		/* Set Timezone */
		date_default_timezone_set("Asia/Jakarta");

		/* Config untuk keperluan title (diambil dari my_config.php) */
		$this->data['_apps_title'] = $this->config->item('tag_apps');
		$this->data['_apps_brand'] = $this->config->item('tag_apps_brand');
		$this->data['_apps_system_name'] = $this->config->item('tag_apps_system');
		$this->data['_apps_company_name'] = $this->config->item('company_copyright');
		$this->data['_apps_company_site'] = $this->config->item('company_site');
		$this->data['_apps_company_city'] = $this->config->item('company_city');

		/* Config untuk keperluan halaman */
		$this->data['_head_title'] = "Blank Page";
		$this->data['_header_title'] = "Blank Page";
		$this->data['_header_sub_title'] = "This is a Blank Page";
		$this->data['_sidebar_focus'] = "";
		$this->data['_notif_script'] = $this->session->flashdata('_notif_script');

		/* Set User Level */
		$this->_set_user_level();

		/* Set Tahun Data */
		$this->_set_tahun_data();
	}

	/* =======================================
	FUNCTION LOAD
	==========================================*/

	/* Function untuk load Dashboard */
	protected function _load_view_dashboard_all($content = 'dashboard')
	{
		/* Load semua file yang dibutuhkan untuk semua bagian halaman kecuali content */
		$this->data['__head_page'] = $this->load->view('components/head', $this->data, TRUE);
		$this->data['__header_page'] = $this->load->view('components/header', $this->data, TRUE);
		$this->data['__script_page'] = $this->load->view('components/script', $this->data, TRUE);
		$this->data['__navbar_page'] = $this->load->view('components/navbar', $this->data, TRUE);

		/* Component by User Level */
		$this->data['__sidebar_page'] = $this->load->view('components/'.$this->session->userdata('user_level').'/sidebar', $this->data, TRUE);

		/* Load Content sesuai kategori user */
		$this->data['__content_page'] = $this->load->view('contents/all/'.$content, $this->data, TRUE);

		/* Load view Dashboard */
		$this->load->view('view_dashboard', $this->data);
	}

	/* Function untuk load Dashboard Member */
	protected function _load_view_dashboard_member($content = 'dashboard')
	{
		/* Load semua file yang dibutuhkan untuk semua bagian halaman kecuali content */
		$this->data['__head_page'] = $this->load->view('components/head', $this->data, TRUE);
		$this->data['__header_page'] = $this->load->view('components/header', $this->data, TRUE);
		$this->data['__script_page'] = $this->load->view('components/script', $this->data, TRUE);
		$this->data['__navbar_page'] = $this->load->view('components/navbar', $this->data, TRUE);

		/* Component by User Level */
		$this->data['__sidebar_page'] = $this->load->view('components/'.$this->session->userdata('user_level').'/sidebar', $this->data, TRUE);

		/* Load Content sesuai kategori user */
		$this->data['__content_page'] = $this->load->view('contents/'.$this->session->userdata('user_level').'/'.$content, $this->data, TRUE);

		/* Load view Dashboard */
		$this->load->view('view_dashboard', $this->data);
	}

	/* Function untuk load only view */
	protected function _load_only_view_all($content = "dashboard")
	{
		/* Load view Dashboard */
		$this->load->view('contents/all/'.$content, $this->data);
	}

	/* Function untuk load only view */
	protected function _load_only_view_member($content = "dashboard")
	{
		/* Load view Dashboard */
		$this->load->view('contents/'.$this->session->userdata('user_level').'/'.$content, $this->data);
	}

	/* Function untuk load View Full Popup */
	protected function _load_view_full_popup($content = 'dashboard')
	{
		/* Load semua file yang dibutuhkan untuk semua bagian halaman kecuali content */
		$this->data['__head_page'] = $this->load->view('components/head', $this->data, TRUE);
		$this->data['__script_page'] = $this->load->view('components/script', $this->data, TRUE);

		/* Load Content sesuai kategori user */
		$this->data['__content_page'] = $this->load->view('contents/popup/'.$content, $this->data, TRUE);

		/* Load view Dashboard */
		$this->load->view('view_full_popup', $this->data);
	}


	/* =======================================
	FUNCTION SETTER
	==========================================*/

	/* Function untuk buat flashdata PopUp status */
	protected function _flashdata_notif($messages = "Tes", $jenis = "success", $layout = "top")
	{
		// $this->session->set_flashdata('_notif_script', "show_notify('".$messages."', '".$jenis."', '".$layout."');");
		$this->session->set_flashdata('_notif_script', "
			$(function() {
				noty({
				    width: 200,
				    text: '".$messages."',
				    type: '".$jenis."',
				    dismissQueue: true,
				    timeout: 3000,
				    layout: '".$layout."'
				});
			});
		");
	}

	/* Config untuk keperluan halaman */
	protected function _set_config_halaman($head_title, $header_title, $header_sub_title, $sidebar_focus)
	{
		/* Set config berdasarkan inputan */
		$this->data['_head_title'] = $head_title;
		$this->data['_header_title'] = $header_title;
		$this->data['_header_sub_title'] = $header_sub_title;
		$this->data['_sidebar_focus'] = $sidebar_focus;
	}

	/* Function daftar level User */
	protected function _set_user_level()
	{
		/* Ambil semua data */
		$daftar_level = $this->sim_user_level_list->get("result_array");

		/* Looping dan masukan ke array */
		$kirim_level = [];
		foreach ($daftar_level as $row) {
			if (!empty($row["user_level_nama"])) {
				$kirim_level[$row["user_level"]]["user_level"] = $row["user_level"];
				$kirim_level[$row["user_level"]]["user_level_nama"] = $row["user_level_nama"];
			}
		}

		/* Set ke data pusat */
		$this->data["_daftar_user_level"] = $kirim_level;
	}

	/* Function set tahun data mulai dan berakhir */
	protected function _set_tahun_data()
	{
		/* Get dari setting database */
		$where_tahun["setting_kode"] = "tahun_mulai";
		$tahun_mulai = $this->sim_setting->get_by($where_tahun, "row_array");
		$this->data['_tahun_mulai'] = $tahun_mulai["setting_value"];

		$where_tahun["setting_kode"] = "tahun_berakhir";
		$tahun_berakhir = $this->sim_setting->get_by($where_tahun, "row_array");
		$this->data['_tahun_berakhir'] = $tahun_berakhir["setting_value"];
	}

}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */