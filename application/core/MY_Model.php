<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {

	/* Config dasar yang diturunkan ke seluruh model yang mengextend */
	protected $_table_name = '';
	protected $_primary_key = '';
	protected $_group_by = '';
	protected $_order_by = '';
	protected $_sequence_name = '';

	/* Fungsi Construct (wajib ada) */
	public function __construct()
	{
		parent::__construct();
		// $this->db->reset_query();
	}

	/* Function to count row */
	public function count_all_row()
	{
		return $this->db->count_all($this->_table_name);
	}

	/* Fungsi untuk get dengan query */
	public function get_by_query($query = NULL,$method = "result",$limit = NULL,$offset = NULL)
	{
		/* Cek apakah query ada atau tidak */
		if ($query == NULL) {
			return NULL;
		}

		/* Cek limit dan offset, jika ada maka set limit dan offset */
		if (($limit != NULL || $limit != '')) {
			$query .= " LIMIT ".$limit." OFFSET ".$offset;
		}

		/* Get berdasarkan query dan kembalikan ke controller */
		return $this->db->query($query)->$method();
	}

	/* Fungsi untuk get otomatis */
	public function get($method = "result_array", $order_by = NULL, $limit = NULL, $offset = 0)
	{
		/* Cek order by, jika ada maka order data */
		if ($order_by != NULL || $order_by != '') {
			$this->db->order_by($order_by);
		} else if($this->_order_by != NULL || $this->_order_by != '') {
			$this->db->order_by($this->_order_by);
		}

		/* Cek limit dan offset, jika ada maka set limit dan offset */
		if (($limit != NULL || $limit != '')) {
			$this->db->limit($limit,$offset);
		}

		/* Get data dan kembalikan ke controller */
		return $this->db->get($this->_table_name)->$method();
	}

	/* Fungsi untuk get dengan where */
	public function get_by($where, $method = 'result_array', $order_by = NULL, $limit = NULL, $offset = 0)
	{
		/* Set where data */
		$this->db->where($where);

		/* Get data melalui fungsi get dan kembalikan ke controller */
		return $this->get($method,$order_by,$limit,$offset);
	}

	/* Fungsi untuk insert dan update data */
	public function save($data, $id = NULL, $where = NULL)
	{

		/* UPDATE by WHERE */
		/* Jika tidak ada id, maka update data yang ada berdasarkan where yang sudah diset */
		if($where != NULL) {
			$this->db->set($data);
			$this->db->where($where);
			$this->db->update($this->_table_name);
		}

		/* UPDATE by ID */
		/* Jika ada id, maka update data yang ada berdasarkan primary key yang sudah ada */
		if($id != NULL) {
			$this->db->set($data);
			$this->db->where($this->_primary_key, $id);
			$this->db->update($this->_table_name);

			/* Return ID */
			return $id;
		}

		/* INSERT */
		/* Jika tidak ada id maka insert data baru */
		if ($id == NULL && $where == NULL) {
			$this->db->set($data);
			$this->db->insert($this->_table_name);

			/* Check sequence */
			if (!empty($this->_sequence_name)) {
				/* Jika ada, kembalikan id yang digenerate */
				return $this->db->insert_id($this->_sequence_name);
			}
		}
	}

	/* Fungsi untuk last query */
	public function get_last_query()
	{
		return $this->db->last_query();
	}

	/* Fungsi untuk insert batch */
	public function save_batch($data)
	{
		/* INSERT BATCH */
		$this->db->insert_batch($this->_table_name, $data);
	}

	/* Fungsi untuk hapus data */
	public function delete($where)
	{
		/* Set Where untuk di delete */
		$this->db->where($where);

		/* Hapus berdasarkan tabel yang ada */
		$this->db->delete($this->_table_name);
	}

}

/* End of file MY_Model.php */
/* Location: ./application/core/MY_Model.php */