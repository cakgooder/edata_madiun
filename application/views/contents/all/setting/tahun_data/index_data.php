
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Form Edit Tahun Data</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        	</ul>
    	</div>
	</div>
	<!-- PANEL BODY -->
	<div class="panel-body">
		<form class="form-horizontal" method="POST" action="<?=base_url("dashboard/setting/tahun-data/simpan/")?>">
			<fieldset class="content-group">
				<legend class="text-bold">Detail Tahun Data</legend>

				<div class="form-group">
					<label class="control-label col-lg-2">Tahun Mulai</label>
					<div class="col-lg-10">
						<select name="tahun_mulai" class="select-search" required="required">
							<option class="bg-grey-300" value="">Pilih Salah Satu</option>
							<?php
			            	for ($i=1989; $i < 2100; $i++) :
			            		if($i == $_tahun_mulai):
							?>
			                <option value="<?=$i?>" selected="selected"><?=$i?></option>
			                <?php
			                	else:
			                ?>
			                <option value="<?=$i?>"><?=$i?></option>
							<?php
								endif;
			            	endfor;
							?>
			            </select>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Tahun Berakhir</label>
					<div class="col-lg-10">
						<select name="tahun_berakhir" class="select-search" required="required">
							<option class="bg-grey-300" value="">Pilih Salah Satu</option>
							<?php
			            	for ($i=1989; $i < 2100; $i++) :
			            		if($i == $_tahun_berakhir):
							?>
			                <option value="<?=$i?>" selected="selected"><?=$i?></option>
			                <?php
			                	else:
			                ?>
			                <option value="<?=$i?>"><?=$i?></option>
							<?php
								endif;
			            	endfor;
							?>
			            </select>
					</div>
				</div>

			</fieldset>

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Simpan</button>
			</div>
		</form>

	</div>
	<!-- END PANEL BODY -->
</div>
<!-- /simple panel -->