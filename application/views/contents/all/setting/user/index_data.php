
<div class="panel panel-flat">
	<div class="panel-body">
		<button type="button" class="btn btn-primary" onclick="location.href='<?=base_url("dashboard/setting/user/tambah")?>';"><i class="icon-plus3 position-left"></i>Tambah Pengguna</button>
	</div>
</div>

<!-- Simple panel -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Daftar Pengguna Sistem</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        	</ul>
    	</div>
	</div>
	<!-- PANEL BODY -->
	<div class="panel-body">

		<div class="table-responsive">
			<table class="table datatable-pagination">
				<thead>
					<tr>
						<th>#</th>
						<th>Username</th>
						<th>Nama Pengguna</th>
						<th>Level</th>
						<th>Status Kunci</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
				<?php
				$i = 1;
				foreach ($daftar_user as $row_user) :
				?>
					<tr id="row-user-<?=$row_user["user_id"]?>">
						<td><?=$i?></td>
						<td><?=$row_user["username"]?></td>
						<td><?=$row_user["user_nama"]?></td>
						<td><?=$_daftar_user_level[$row_user["user_level"]]["user_level_nama"]?></td>
						<td>
						<?php
						if($row_user["locked"] == "locked") :
						?>
							<span class="label bg-danger-600 status-unlock-user-<?=$row_user["user_id"]?>" data-popup="tooltip" data-placement="top" title="Status User : Terkunci">Terkunci</span>
							<span style="display: none;" class="label bg-teal-600 status-lock-user-<?=$row_user["user_id"]?>" data-popup="tooltip" data-placement="top" title="Status User : Tidak Terkunci">Tidak Terkunci</span>
						<?php
						else :
						?>
							<span style="display: none;" class="label bg-danger-600 status-unlock-user-<?=$row_user["user_id"]?>" data-popup="tooltip" data-placement="top" title="Status User : Terkunci">Terkunci</span>
							<span class="label bg-teal-600 status-lock-user-<?=$row_user["user_id"]?>" data-popup="tooltip" data-placement="top" title="Status User : Tidak Terkunci">Tidak Terkunci</span>
						<?php
						endif;
						?>
						</td>
						<td>
							<div class="btn-group-vertical">
								<div class="btn-group">
									<button type="button" class="btn btn-primary btn-icon" data-popup="tooltip" title="Edit User" data-placement="top" onclick="location.href='<?=base_url("dashboard/setting/user/edit/".encrypting_code($row_user["user_id"]))?>';"><i class="icon-wrench"></i></button>
									<button type="button" class="btn btn-danger btn-icon btn-hapus-user" data-popup="tooltip" title="Hapus User" data-placement="top" data-user-id="<?=encrypting_code($row_user["user_id"])?>"><i class="icon-trash"></i></button>
									<?php
									if($row_user["locked"] == "locked") :
									?>
										<button style="display: none;" type="button" class="btn btn-warning btn-icon btn-lock-user status-lock-user-<?=$row_user["user_id"]?>" id="" data-popup="tooltip" title="Kunci User" data-placement="top" data-user-id="<?=$row_user["user_id"]?>" data-status="locked"><i class="icon-lock"></i></button>
										<button type="button" class="btn btn-success btn-icon btn-lock-user status-unlock-user-<?=$row_user["user_id"]?>" id="" data-popup="tooltip" title="Buka Kunci User" data-placement="top" data-user-id="<?=$row_user["user_id"]?>" data-status="unlocked"><i class="icon-key"></i></button>
									<?php
									else :
									?>
										<button type="button" class="btn btn-warning btn-icon btn-lock-user status-lock-user-<?=$row_user["user_id"]?>" id="" data-popup="tooltip" title="Kunci User" data-placement="top"  data-user-id="<?=$row_user["user_id"]?>" data-status="locked"><i class="icon-lock"></i></button>
										<button style="display: none;" type="button" class="btn btn-success btn-icon btn-lock-user status-unlock-user-<?=$row_user["user_id"]?>" id="" data-popup="tooltip" title="Buka Kunci User" data-placement="top" data-user-id="<?=$row_user["user_id"]?>" data-status="unlocked"><i class="icon-key"></i></button>
									<?php
									endif;
									?>
									
								</div>
							</div>
						</td>
					</tr>
				<?php
					$i++;
				endforeach;
				?>
				</tbody>
			</table>
		</div>

	</div>
	<!-- END PANEL BODY -->
</div>
<!-- /simple panel -->

<!-- Modal Hapus User -->
<div id="modal-hapus-user" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-danger">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h6 class="modal-title" id="modal-title-hapus-user">Hapus User</h6>
			</div>
			<form id="form-hapus-user" action="<?=base_url("dashboard/setting/user/proses-hapus")?>" method="POST">
			<div class="modal-body">
				<h6 class="text-semibold" id="modal-subtitle-hapus-user">Apakah yakin ingin hapus user?</h6>
				<p>User yang dihapus akan hilang dari sistem dan tidak akan bisa dikembalikan. </p>
				<hr>
				<p>Jika ingin membatalkan permintaan, anda bisa mengklik tombol Batal.</p>
			</div>

			<input type="hidden" class="input-user-id" name="user_id_enc">
			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-danger" id="btn-proses-hapus-user">Proses</button>
			</div>
			</form>
		</div>
	</div>
</div>

<!-- Modal Locked User -->
<div id="modal-lock-user" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-warning">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h6 class="modal-title" id="modal-title-lock-user">Kunci User</h6>
			</div>
			<form id="form-lock-user" action="<?=base_url("dashboard/setting/user/proses-lock")?>" method="POST">
			<div class="modal-body">
				<h6 class="text-semibold" id="modal-subtitle-lock-user">Apakah yakin ingin kunci user?</h6>
				<p>User yang dikunci <b>tidak akan bisa login</b> ke dalam sistem. </p>
				<hr>
				<p>Jika ingin membatalkan permintaan, anda bisa mengklik tombol Batal.</p>
			</div>

			<input type="hidden" class="input-user-id" name="user_id">
			<input type="hidden" class="input-status" name="status">
			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-warning" id="btn-proses-lock-user">Proses</button>
			</div>
			</form>
		</div>
	</div>
</div>

<!-- Modal UnLocked User -->
<div id="modal-unlock-user" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-success">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h6 class="modal-title" id="modal-title-lock-user">Buka Kunci User</h6>
			</div>
			<form id="form-unlock-user" action="<?=base_url("dashboard/setting/user/proses-lock")?>" method="POST">
			<div class="modal-body">
				<h6 class="text-semibold" id="modal-subtitle-lock-user">Apakah yakin ingin buka kunci user?</h6>
				<p>User yang dibuka kunci <b>akan bisa login</b> ke dalam sistem. </p>
				<hr>
				<p>Jika ingin membatalkan permintaan, anda bisa mengklik tombol Batal.</p>
			</div>

			<input type="hidden" class="input-user-id" name="user_id">
			<input type="hidden" class="input-status" name="status">
			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-success" id="btn-proses-unlock-user">Proses</button>
			</div>
			</form>
		</div>
	</div>
</div>
