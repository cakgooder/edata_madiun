
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Form Edit User</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        	</ul>
    	</div>
	</div>
	<!-- PANEL BODY -->
	<div class="panel-body">
		<p><span class="text-danger">*</span> Wajib Diisi</p>

		<form class="form-horizontal" method="POST" action="<?=base_url("dashboard/setting/user/proses-edit/".encrypting_code($data_user["user_id"]))?>">
			<fieldset class="content-group">
				<legend class="text-bold">Data User</legend>

				<div class="form-group">
					<label class="control-label col-lg-2">Username <span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<input type="text" name="username" value="<?=$data_user["username"]?>" class="form-control" required="required">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Password </label>
					<div class="col-lg-10">
						<input type="text" name="password" value="" class="form-control" placeholder="Kosongkan jika tidak ingin merubah password">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Nama User <span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<input type="text" name="user_nama" value="<?=$data_user["user_nama"]?>" class="form-control" required="required">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">User level <span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<select name="user_level" class="select" required="required">
							<option class="bg-grey" value="">Pilih Salah Satu</option>
                        <?php
                        foreach($_daftar_user_level as $user_level => $row_level) :
                        ?>
                            <option value="<?=$user_level?>" <?php if($data_user["user_level"] == $user_level) echo "selected"; ?>><?=$user_level?> - <?=$row_level["user_level_nama"]?></option>
                        <?php
                        endforeach;
                        ?>
                        </select>
					</div>
				</div>

			</fieldset>

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Simpan</button>
				<button type="button" class="btn btn-default" onclick="event.preventDefault();location.href='<?=base_url("dashboard/setting/user/")?>';">Kembali</button>
			</div>
		</form>

	</div>
	<!-- END PANEL BODY -->
</div>
<!-- /simple panel -->