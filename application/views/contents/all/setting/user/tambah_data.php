
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Form Tambah User</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        	</ul>
    	</div>
	</div>
	<!-- PANEL BODY -->
	<div class="panel-body">
		<p><span class="text-danger">*</span> Wajib Diisi</p>

		<form class="form-horizontal" method="POST" action="<?=base_url("dashboard/setting/user/proses-tambah/")?>">
			<fieldset class="content-group">
				<legend class="text-bold">Data User</legend>

				<div class="form-group">
					<label class="control-label col-lg-2">Username <span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<input type="text" name="username" value="" class="form-control" required="required">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Password <span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<input type="text" name="password" value="" class="form-control" required="required">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Nama User <span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<input type="text" name="user_nama" value="" class="form-control" required="required">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">User level <span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<select name="user_level" class="select" required="required">
							<option class="bg-grey" value="">Pilih Salah Satu</option>
                        <?php
                        foreach($_daftar_user_level as $user_level => $row_level) :
                        ?>
                            <option value="<?=$user_level?>"><?=$user_level?> - <?=$row_level["user_level_nama"]?></option>
                        <?php
                        endforeach;
                        ?>
                        </select>
					</div>
				</div>

			</fieldset>

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Simpan</button>
				<button type="button" class="btn btn-default" onclick="event.preventDefault();location.href='<?=base_url("dashboard/setting/user/")?>';">Kembali</button>
			</div>
		</form>

	</div>
	<!-- END PANEL BODY -->
</div>
<!-- /simple panel -->