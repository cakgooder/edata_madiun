
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Form Tambah Data Atom</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        	</ul>
    	</div>
	</div>
	<!-- PANEL BODY -->
	<div class="panel-body">
		<p><span class="text-danger">*</span> Wajib Diisi</p>

		<form class="form-horizontal" method="POST" action="<?=base_url("dashboard/setting/data-atom/proses-tambah/")?>">
			<fieldset class="content-group">
				<legend class="text-bold">Detail Data Atom</legend>

				<div class="form-group">
					<label class="control-label col-lg-2">Nama Data Atom <span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<input type="text" name="data_atom_narasi" value="" class="form-control" required="required">
					</div>
				</div>

				<div id="if-not-angka">
					<div class="form-group">
						<label class="control-label col-lg-2">Data Master <span class="text-danger">*</span></label>
						<div class="col-lg-10">
							<select name="data_master_id" class="select-search" id="select-data-atom-data-master">
								<option class="bg-grey-300" value="">Pilih Salah Satu</option>
<?php
				            	foreach($daftar_urusan as $row_urusan):
				            		foreach($row_urusan["daftar_data_master"] as $row_data_master) :
?>
				                <option value="<?=$row_data_master["data_master_id"]?>">(<?=$row_urusan["urusan_kode"]?>) <?=$row_urusan["urusan_narasi"]?> - (<?=$row_data_master["data_master_id"]?>) <?=$row_data_master["data_master_nama"]?></option>
<?php
									endforeach;
				            	endforeach;
?>
				            </select>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Satuan Data Atom <span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<input type="text" name="data_atom_satuan" value="" class="form-control" required="required">
					</div>
				</div>

				<div class="row" id="loading-proses" style="display: none;">
					<i class="icon-spinner9 spinner"></i> Tunggu Sebentar
				</div>

			</fieldset>

			<fieldset class="content-group" id="body-set-realisasi">
				
			</fieldset>


			<div class="form-group">
				<button type="submit" class="btn btn-primary">Simpan</button>
				<button type="button" class="btn btn-default" onclick="event.preventDefault();location.href='<?=base_url("dashboard/setting/data-atom/")?>';">Kembali</button>
			</div>
		</form>

	</div>
	<!-- END PANEL BODY -->
</div>
<!-- /simple panel -->
