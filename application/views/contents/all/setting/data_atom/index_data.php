
<div class="panel panel-flat">
	<div class="panel-body">
		<button type="button" class="btn btn-primary" onclick="location.href='<?=base_url("dashboard/setting/data-atom/tambah")?>';"><i class="icon-plus3 position-left"></i>Tambah Data Atom</button>
	</div>
</div>


<!-- Simple panel -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Pilih Data Master</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        	</ul>
    	</div>
	</div>
	<!-- PANEL BODY -->
	<div class="panel-body" id="forms-target-left">
		<form class="form-horizontal form-refresh-filter" method="POST" action="<?=base_url("dashboard/setting/data-atom/filter-data-master/")?>">

			<fieldset class="content-group">

				<div class="form-group">
					<label class="control-label col-lg-2">Data Master</label>
					<div class="col-lg-10">
						<select name="data_master_id_enc" class="select-search" id="select-filter-urusan" required="required">
							<option class="bg-grey-300" value="">Pilih Salah Satu</option>
<?php
			            	foreach($daftar_urusan as $row_urusan):
			            		foreach($row_urusan["daftar_data_master"] as $row_data_master) :
?>
			                <option value="<?=encrypting_code($row_data_master["data_master_id"])?>">(<?=$row_urusan["urusan_kode"]?>) <?=$row_urusan["urusan_narasi"]?> - (<?=$row_data_master["data_master_id"]?>) <?=$row_data_master["data_master_nama"]?></option>
<?php
								endforeach;
			            	endforeach;
?>
			            </select>
					</div>
				</div>

				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-refresh-filter" data-to-refresh="refresh-filter">Refresh</button>
				</div>

			</fieldset>

		</form>

	</div>
	<!-- END PANEL BODY -->
</div>

<!-- Simple panel -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Hasil Filter per Data Master</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        	</ul>
    	</div>
	</div>
	<!-- PANEL BODY -->
	<div class="panel-body hasil-refresh-filter" id="">
	</div>
	<div class="panel-body loading-refresh-filter" id="" style="display:none">
		<i class="icon-spinner9 spinner"></i> Tunggu Sebentar
	</div>
	<!-- END PANEL BODY -->
</div>
<!-- /simple panel -->


<!-- Modal Hapus -->
<div id="modal-hapus-data-atom" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-danger">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h6 class="modal-title" id="modal-title-hapus-data-atom">Hapus Data Atom</h6>
			</div>
			<form id="form-hapus-data-atom" action="<?=base_url("dashboard/setting/data-atom/proses-hapus")?>" method="POST">
			<div class="modal-body">
				<h6 class="text-semibold" id="modal-hapus-data-atom">Apakah yakin ingin hapus Data Atom?</h6>
				<p>Data Atom yang dihapus akan hilang dari sistem dan tidak akan bisa dikembalikan. </p>
				<hr>
				<p>Jika ingin membatalkan permintaan, anda bisa mengklik tombol Batal.</p>
			</div>

			<input type="hidden" class="input-id-data-atom" name="data_atom_id">
			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">Batal</button>
				<button type="submit" class="btn btn-danger" id="btn-proses-hapus-data-atom">Proses</button>
			</div>
			</form>
		</div>
	</div>
</div>