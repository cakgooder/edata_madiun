
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Form Tambah Data Atom</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        	</ul>
    	</div>
	</div>
	<!-- PANEL BODY -->
	<div class="panel-body">
		<p><span class="text-danger">*</span> Wajib Diisi</p>

		<form class="form-horizontal" method="POST" action="<?=base_url("dashboard/setting/data-atom/proses-edit/".encrypting_code($detail_data_atom["data_atom_id"]))?>">
			<fieldset class="content-group">
				<legend class="text-bold">Detail Data Atom</legend>

				<div class="form-group">
					<label class="control-label col-lg-2">Nama Data Atom <span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<input type="text" name="data_atom_narasi" value="<?=$detail_data_atom["data_atom_narasi"]?>" class="form-control" required="required">
					</div>
				</div>

				<div id="if-not-angka">
					<div class="form-group">
						<label class="control-label col-lg-2">Data Master <span class="text-danger">*</span></label>
						<div class="col-lg-10">
							<select name="data_master_id" class="select-search" id="select-data-atom-data-master">
								<option class="bg-grey-300" value="">Pilih Salah Satu</option>
<?php
				            	foreach($daftar_urusan as $row_urusan):
				            		foreach($row_urusan["daftar_data_master"] as $row_data_master) :
				            			if($detail_data_atom["data_master_id"] == $row_data_master["data_master_id"]):
?>
				                <option value="<?=$row_data_master["data_master_id"]?>" selected="selected">(<?=$row_urusan["urusan_kode"]?>) <?=$row_urusan["urusan_narasi"]?> - (<?=$row_data_master["data_master_id"]?>) <?=$row_data_master["data_master_nama"]?></option>
				                <?php
				                		else :
				                ?>
				                <option value="<?=$row_data_master["data_master_id"]?>">(<?=$row_urusan["urusan_kode"]?>) <?=$row_urusan["urusan_narasi"]?> - (<?=$row_data_master["data_master_id"]?>) <?=$row_data_master["data_master_nama"]?></option>
<?php
										endif;
									endforeach;
				            	endforeach;
?>
				            </select>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Satuan Data Atom <span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<input type="text" name="data_atom_satuan" value="<?=$detail_data_atom["data_atom_satuan"]?>" class="form-control" required="required">
					</div>
				</div>

				<div class="row" id="loading-proses" style="display: none;">
					<i class="icon-spinner9 spinner"></i> Tunggu Sebentar
				</div>

			</fieldset>

			<fieldset class="content-group" id="body-set-realisasi">
				<legend class="text-bold">Set Realisasi </legend>

				<div class="form-group">
					<label class="control-label col-lg-2">Kolom Realisasi Awal </label>
					<div class="col-lg-10">
						<select name="data_atom_kolom_realisasi_awal" class="select-search">
							<option class="bg-grey" value="">Pilih Salah Satu</option>
							<?php
							foreach($daftar_kolom as $row_kolom):
								if($detail_data_atom["data_atom_kolom_realisasi_awal"] == $row_kolom["kolom_ke"]):
							?>
				            <option value="<?=$row_kolom["kolom_ke"]?>" selected="selected">(<?=$row_kolom["kolom_type_data"]?>) <?=$row_kolom["kolom_ke"]?> - <?=$row_kolom["kolom_nama"]?></option>
				            <?php
				            	else :
				            ?>
				            <option value="<?=$row_kolom["kolom_ke"]?>">(<?=$row_kolom["kolom_type_data"]?>) <?=$row_kolom["kolom_ke"]?> - <?=$row_kolom["kolom_nama"]?></option>
							<?php
								endif;
							endforeach;
							?>
				        </select>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Kolom Realisasi Akhir </label>
					<div class="col-lg-10">
						<select name="data_atom_kolom_realisasi_akhir" class="select-search">
							<option class="bg-grey" value="">Pilih Salah Satu</option>
							<?php
							foreach($daftar_kolom as $row_kolom):
								if($detail_data_atom["data_atom_kolom_realisasi_akhir"] == $row_kolom["kolom_ke"]):
							?>
				            <option value="<?=$row_kolom["kolom_ke"]?>" selected="selected">(<?=$row_kolom["kolom_type_data"]?>) <?=$row_kolom["kolom_ke"]?> - <?=$row_kolom["kolom_nama"]?></option>
				            <?php
				            	else :
				            ?>
				            <option value="<?=$row_kolom["kolom_ke"]?>">(<?=$row_kolom["kolom_type_data"]?>) <?=$row_kolom["kolom_ke"]?> - <?=$row_kolom["kolom_nama"]?></option>
							<?php
								endif;
							endforeach;
							?>
				        </select>
						<span class="help-block">Isikan jika realisasi memiliki range tertentu. Jika <u>tidak ada range</u>, cukup isi <b>Kolom Awal</b> saja.</span>
					</div>
				</div>

				<div class="form-group" id="jenis-realisasi-kolom">
					<label class="control-label col-lg-2">Jenis Realisasi <span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<select name="data_atom_jenis_realisasi" class="select-search">
							<option class="bg-grey-300" value="">Pilih Salah Satu</option>
							<?php
							foreach($daftar_jenis_realisasi as $key_jenis => $row_jenis_realisasi):
								if($detail_data_atom["data_atom_jenis_realisasi"] == $key_jenis):
							?>
				            <option value="<?=$key_jenis?>" selected="selected"><?=$row_jenis_realisasi?></option>
				            <?php
				            	else :
				            ?>
				            <option value="<?=$key_jenis?>"><?=$row_jenis_realisasi?></option>
							<?php
								endif;
							endforeach;
							?>
				        </select>
					</div>
				</div>

				<script type="text/javascript" src="<?=base_url("assets/js/plugins/forms/styling/uniform.min.js")?>"></script>
				<script type="text/javascript" src="<?=base_url("assets/js/plugins/forms/inputs/touchspin.min.js")?>"></script>
				<script type="text/javascript" src="<?=base_url("assets/js/plugins/forms/selects/select2.min.js")?>"></script>
				<script type="text/javascript" src="<?=base_url("assets/js/pages/form_input_groups.js")?>"></script>
				<script type="text/javascript" src="<?=base_url("assets/js/pages/form_select2.js")?>"></script>

			</fieldset>


			<div class="form-group">
				<button type="submit" class="btn btn-primary">Simpan</button>
				<button type="button" class="btn btn-default" onclick="event.preventDefault();location.href='<?=base_url("dashboard/setting/data-atom/")?>';">Kembali</button>
			</div>
		</form>

	</div>
	<!-- END PANEL BODY -->
</div>
<!-- /simple panel -->
