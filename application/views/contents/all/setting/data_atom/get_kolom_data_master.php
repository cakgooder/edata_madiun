<legend class="text-bold">Set Realisasi </legend>

<div class="form-group">
	<label class="control-label col-lg-2">Kolom Realisasi Awal </label>
	<div class="col-lg-10">
		<select name="data_atom_kolom_realisasi_awal" class="select-search">
			<option class="bg-grey" value="">Pilih Salah Satu</option>
			<?php
			foreach($daftar_kolom as $row_kolom):
			?>
            <option value="<?=$row_kolom["kolom_ke"]?>">(<?=$row_kolom["kolom_type_data"]?>) <?=$row_kolom["kolom_ke"]?> - <?=$row_kolom["kolom_nama"]?></option>
			<?php
			endforeach;
			?>
        </select>
	</div>
</div>

<div class="form-group">
	<label class="control-label col-lg-2">Kolom Realisasi Akhir </label>
	<div class="col-lg-10">
		<select name="data_atom_kolom_realisasi_akhir" class="select-search">
			<option class="bg-grey" value="">Pilih Salah Satu</option>
			<?php
			foreach($daftar_kolom as $row_kolom):
			?>
            <option value="<?=$row_kolom["kolom_ke"]?>">(<?=$row_kolom["kolom_type_data"]?>) <?=$row_kolom["kolom_ke"]?> - <?=$row_kolom["kolom_nama"]?></option>
			<?php
			endforeach;
			?>
        </select>
		<span class="help-block">Isikan jika realisasi memiliki range tertentu. Jika <u>tidak ada range</u>, cukup isi <b>Kolom Awal</b> saja.</span>
	</div>
</div>

<div class="form-group" id="jenis-realisasi-kolom">
	<label class="control-label col-lg-2">Jenis Realisasi <span class="text-danger">*</span></label>
	<div class="col-lg-10">
		<select name="data_atom_jenis_realisasi" class="select-search">
			<option class="bg-grey-300" value="">Pilih Salah Satu</option>
			<?php
			foreach($daftar_jenis_realisasi as $key_jenis => $row_jenis_realisasi):
			?>
            <option value="<?=$key_jenis?>"><?=$row_jenis_realisasi?></option>
			<?php
			endforeach;
			?>
        </select>
	</div>
</div>

<script type="text/javascript" src="<?=base_url("assets/js/plugins/forms/styling/uniform.min.js")?>"></script>
<script type="text/javascript" src="<?=base_url("assets/js/plugins/forms/inputs/touchspin.min.js")?>"></script>
<script type="text/javascript" src="<?=base_url("assets/js/plugins/forms/selects/select2.min.js")?>"></script>
<script type="text/javascript" src="<?=base_url("assets/js/pages/form_input_groups.js")?>"></script>
<script type="text/javascript" src="<?=base_url("assets/js/pages/form_select2.js")?>"></script>
