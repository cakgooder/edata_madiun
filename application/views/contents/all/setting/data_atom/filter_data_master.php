<!-- TABLE SUB -->
<fieldset class="content-group">
	<legend class="text-bold">Daftar Rawdata</legend>
	<p>Keterangan Warna Tahun :
		<?php for($i = $_tahun_mulai; $i <= $_tahun_berakhir; $i++): ?>
			<span class="label bg-<?=get_color_by_tahun($i)?>" data-popup="tooltip" data-placement="top" title="Data Tahun <?=$i?>">Data Tahun <?=$i?></span>
		<?php endfor; ?>
	</p>
	<div class="table-responsive">
		<table class="table table-bordered">
			<thead>
				<tr class="border-solid">
					<th>Data Atom ID</th>
					<th>Narasi Data Atom</th>
					<th>Aksi</th>

				</tr>
			</thead>
			<tbody>
				<?php
				foreach($daftar_data_atom as $row_data_atom) :
				?>
				<tr id="row-data-atom-<?=$row_data_atom["data_atom_id"]?>">
					<td><?=$row_data_atom["data_atom_id"]?></td>
					<td><?=$row_data_atom["data_atom_narasi"]?></td>
					<td>
						<div class="btn-group">
							<!-- Button Edit -->
							<button type="button" class="btn btn-primary btn-icon" data-popup="tooltip" title="Edit Data Atom" data-placement="top" onclick="location.href='<?=base_url("dashboard/setting/data-atom/edit/".encrypting_code($row_data_atom["data_atom_id"]))?>';"><i class="icon-wrench"></i></button>
							<!-- Button Hapus -->
							<button type="button" class="btn btn-danger btn-icon btn-hapus-data-atom" data-popup="tooltip" title="Hapus Data Atom" data-placement="top" data-id-data-atom="<?=encrypting_code($row_data_atom["data_atom_id"])?>"><i class="icon-trash"></i></button>
						</div>
					</td>
				</tr>
				<?php
				endforeach;
				?>
			</tbody>
		</table>
	</div>
</fieldset>
<!-- END TABLE SUB -->

<script type="text/javascript">
// Tooltip
$('[data-popup="tooltip"]').tooltip();
</script>
