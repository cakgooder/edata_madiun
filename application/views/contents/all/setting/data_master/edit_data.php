
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Form Edit Data Master</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        	</ul>
    	</div>
	</div>
	<!-- PANEL BODY -->
	<div class="panel-body">
		<p><span class="text-danger">*</span> Wajib Diisi</p>

		<form class="form-horizontal" method="POST" action="<?=base_url("dashboard/setting/data-master/proses-edit/".encrypting_code($row_data_master["data_master_id"]))?>">
			<fieldset class="content-group">
				<legend class="text-bold">Detail Data Master</legend>
				<input type="hidden" name="urutan_kolom_key" value="<?php foreach ($daftar_kolom as $row_kolom){ echo $row_kolom["kolom_ke"].";"; }?>">

				<div class="form-group">
					<label class="control-label col-lg-2">Nama Data Master <span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<input type="text" name="data_master_nama" value="<?=$row_data_master["data_master_nama"]?>" class="form-control" required="required">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Urusan</label>
					<div class="col-lg-10">
						<select name="urusan_kode" class="select-search" required="required">
							<?php
			            	foreach($daftar_urusan as $row_urusan):
			            		if($row_urusan["urusan_kode"] == $row_data_master["urusan_kode"]):
							?>
			                <option value="<?=$row_urusan["urusan_kode"]?>" selected="selected"><?=$row_urusan["urusan_narasi"]?></option>
			                <?php
			                	else :
			                ?>
			                <option value="<?=$row_urusan["urusan_kode"]?>"><?=$row_urusan["urusan_narasi"]?></option>
							<?php
								endif;
			            	endforeach;
							?>
			            </select>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Ket Data Master </label>
					<div class="col-lg-10">
						<input type="text" name="data_master_ket" value="<?=$row_data_master["data_master_ket"]?>" class="form-control">
					</div>
				</div>

			</fieldset>
			<p>Drag and Drop Kolom Header untuk merubah urutan kolom</p>

			<div class="row" id="kolom-data">
				
					<?php
					foreach ($daftar_kolom as $row_kolom) :
					?>
				<div class="panel-group panel-group-control content-group-lg kolom-key-<?=$row_kolom["kolom_ke"]?>" id="accordion-kegiatan" data-id-kolom="<?=$row_kolom["kolom_ke"]?>">
					<div class="panel">
						<div class="panel-heading bg-blue-800">
							<h6 class="panel-title">
								<a class="collapsed" data-toggle="collapse" data-parent="#accordion-kolom" href="#accordion-kolom-ke-<?=$row_kolom["kolom_ke"]?>"></a>Kolom Header <?=$row_kolom["kolom_ke"]?> (Drag and Drop) | <button class="btn btn-danger btn-icon btn-rounded btn-sm btn-remove-kolom" data-popup="tooltip" data-placement="top"  data-tobe-removed=".kolom-key-<?=$row_kolom["kolom_ke"]?>" title="Hapus Kolom Ini"><i class="icon-trash"></i></button>
							</h6>
						</div>
						<div id="accordion-kolom-ke-<?=$row_kolom["kolom_ke"]?>" class="panel-collapse collapse">
							<div class="panel-body">
								<fieldset class="content-group">

									<div class="form-group">
										<label class="control-label col-lg-2">Nama Kolom <span class="text-danger">*</span></label>
										<div class="col-lg-10">
											<input type="text" name="data_kolom[<?=$row_kolom["kolom_ke"]?>][kolom_nama]" value="<?=$row_kolom["kolom_nama"]?>" class="form-control" placeholder="Isikan nama dari kolom ini" required="required">
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-lg-2">Kode Kolom <span class="text-danger">*</span></label>
										<div class="col-lg-10">
											<input type="text" name="data_kolom[<?=$row_kolom["kolom_ke"]?>][kolom_kode]" value="<?=$row_kolom["kolom_kode"]?>" class="form-control" placeholder="Isikan kode untuk kolom." required="required">
											<span class="help-block">Tanpa spasi, tanpa huruf besar.</span>
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-lg-2">Tipe Data <span class="text-danger">*</span></label>
										<div class="col-lg-10">
											<select name="data_kolom[<?=$row_kolom["kolom_ke"]?>][kolom_type_data]" class="form-control" required="required">
												<option class="bg-grey-300" value="">Pilih Salah Satu</option>
								            	<?php
								            	foreach($daftar_tipe_data as $key_tipe => $value_tipe):
								            		if($key_tipe == $row_kolom["kolom_type_data"]):
								            	?>
								                	<option value="<?=$key_tipe?>" selected="selected"><?=$value_tipe?></option>
								            	<?php
								            		else :
								            	?>
								                	<option value="<?=$key_tipe?>"><?=$value_tipe?></option>
								            	<?php
								            		endif;
								            	endforeach;
								            	?>
								            </select>
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-lg-2">Khusus Boolean </label>
										<div class="col-lg-10">
											<input type="text" name="data_kolom[<?=$row_kolom["kolom_ke"]?>][kolom_boolean]" value="<?=$row_kolom["kolom_boolean"]?>" class="form-control" placeholder="Isikan yang wajib diisi dengan dipisah ;. Contoh : negeri;swasta">
											<span class="help-block">Diisi jika tipe data Boolean. Isikan yang wajib diisi dengan dipisah ;. Contoh : negeri;swasta</span>
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-lg-2">Keterangan Kolom <span class="text-danger">*</span></label>
										<div class="col-lg-10">
											<input type="text" name="data_kolom[<?=$row_kolom["kolom_ke"]?>][kolom_ket]" value="<?=$row_kolom["kolom_ket"]?>" class="form-control" placeholder="Isikan keterangan untuk kolom. Maksimal 500 character" required="required">
										</div>
									</div>

								</fieldset>
							</div>
						</div>
					</div>
				</div>
	<?php
					endforeach;
	?>
					<script type="text/javascript" src="<?=base_url("assets/js/plugins/forms/inputs/touchspin.min.js")?>"></script>
					<script type="text/javascript" src="<?=base_url("assets/js/pages/form_input_groups.js")?>"></script>
			</div>

			<div class="form-group">
				<button class="btn bg-success-700" id="btn-data-tambah-kolom"><i class="icon-spinner spinner position-left" id="loading-tambah-kolom-data" style="display: none;"></i> Tambah Kolom</button>
			</div>

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Simpan</button>
				<button type="button" class="btn btn-default" onclick="event.preventDefault();location.href='<?=base_url("dashboard/setting/data-master/")?>';">Kembali</button>
			</div>
		</form>

	</div>
	<!-- END PANEL BODY -->
</div>
<!-- /simple panel -->

<!-- Modal Hapus User -->
<div id="modal-hapus-kolom" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-danger">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h6 class="modal-title">Hapus Kolom</h6>
			</div>
			<input type="hidden" name="hapus_proses" value="">

			<div class="modal-body">
				<h6 class="text-semibold">Apakah yakin ingin menghapus Kolom ini dari Data Master ini?</h6>
				<p>Jika anda ingin menambahkan kolom kembali, tambahkan ulang melalui form.</p>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-danger" id="btn-yakin-remove-kolom">Yakin</button>
			</div>
		</div>
	</div>
</div>