<div class="panel-group panel-group-control content-group-lg kolom-key-<?=$kolom_key?>" id="accordion-kegiatan" data-id-kolom="<?=$kolom_key?>">
	<div class="panel">
		<div class="panel-heading bg-blue-800">
			<h6 class="panel-title">
				<a class="collapsed" data-toggle="collapse" data-parent="#accordion-kolom" href="#accordion-kolom-ke-<?=$kolom_key?>"></a>Kolom Header <?=$kolom_key?> (Drag and Drop) | <button class="btn btn-danger btn-icon btn-rounded btn-sm btn-remove-kolom" data-popup="tooltip" data-placement="top"  data-tobe-removed=".kolom-key-<?=$kolom_key?>" title="Hapus Kolom Ini"><i class="icon-trash"></i></button>
			</h6>
		</div>
		<div id="accordion-kolom-ke-<?=$kolom_key?>" class="panel-collapse collapse">
			<div class="panel-body">
				<fieldset class="content-group">

					<div class="form-group">
						<label class="control-label col-lg-2">Nama Kolom <span class="text-danger">*</span></label>
						<div class="col-lg-10">
							<input type="text" name="data_kolom[<?=$kolom_key?>][kolom_nama]" value="" class="form-control" placeholder="Isikan nama dari kolom ini" required="required">
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-lg-2">Kode Kolom <span class="text-danger">*</span></label>
						<div class="col-lg-10">
							<input type="text" name="data_kolom[<?=$kolom_key?>][kolom_kode]" value="" class="form-control" placeholder="Isikan kode untuk kolom." required="required">
							<span class="help-block">Tanpa spasi, tanpa huruf besar.</span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-lg-2">Tipe Data <span class="text-danger">*</span></label>
						<div class="col-lg-10">
							<select name="data_kolom[<?=$kolom_key?>][kolom_type_data]" class="form-control" required="required">
								<option class="bg-grey-300" value="">Pilih Salah Satu</option>
				            	<?php
				            	foreach($daftar_tipe_data as $key_tipe => $value_tipe):
				            	?>
				                <option value="<?=$key_tipe?>"><?=$value_tipe?></option>
				            	<?php
				            	endforeach;
				            	?>
				            </select>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-lg-2">Khusus Boolean </label>
						<div class="col-lg-10">
							<input type="text" name="data_kolom[<?=$kolom_key?>][kolom_boolean]" value="" class="form-control" placeholder="Isikan yang wajib diisi dengan dipisah ;. Contoh : negeri;swasta">
							<span class="help-block">Diisi jika tipe data Boolean. Isikan yang wajib diisi dengan dipisah ;. Contoh : negeri;swasta</span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-lg-2">Keterangan Kolom <span class="text-danger">*</span></label>
						<div class="col-lg-10">
							<input type="text" name="data_kolom[<?=$kolom_key?>][kolom_ket]" value="" class="form-control" placeholder="Isikan keterangan untuk kolom. Maksimal 500 character" required="required">
						</div>
					</div>


				</fieldset>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?=base_url("assets/js/plugins/forms/inputs/touchspin.min.js")?>"></script>
<script type="text/javascript" src="<?=base_url("assets/js/pages/form_input_groups.js")?>"></script>