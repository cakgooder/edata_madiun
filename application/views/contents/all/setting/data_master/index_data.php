<div class="panel panel-flat">
	<div class="panel-body">
		<button type="button" class="btn btn-success" onclick="location.href='<?=base_url("dashboard/setting/data-master/tambah")?>';"><i class="icon-plus3 position-left"></i>Tambah Data Master</button>
	</div>
</div>

<!-- Simple panel -->
<div class="panel panel-flat">
	<!-- PANEL BODY -->
	<div class="panel-body" id="forms-target-left">
		<form class="form-horizontal form-refresh-filter" method="POST" action="<?=base_url("dashboard/setting/data-master/proses-filter-urusan")?>">

			<fieldset class="content-group">

				<div class="form-group">
					<label class="control-label col-lg-2">Urusan</label>
					<div class="col-lg-10">
						<select name="urusan_kode" class="select-search" id="" required="required">
							<option class="bg-grey-300" value="">Pilih Salah Satu</option>
							<?php foreach ($list_urusan as $row_urusan): ?>
								<option value="<?=$row_urusan["urusan_kode"]?>"><?=$row_urusan["urusan_narasi"]?></option>
							<?php endforeach; ?>
			            </select>
					</div>
				</div>

				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-refresh-filter" data-to-refresh="refresh-filter">Refresh</button>
				</div>

			</fieldset>

		</form>

	</div>
	<!-- END PANEL BODY -->
</div>

<!-- Simple panel -->
<div class="panel panel-flat">
	<!-- PANEL BODY -->
	<div class="panel-body hasil-refresh-filter" id="">
	</div>
	<div class="panel-body loading-refresh-filter" id="" style="display:none">
		<i class="icon-spinner9 spinner"></i> Tunggu Sebentar
	</div>
	<!-- END PANEL BODY -->
</div>
<!-- /simple panel -->

<!-- Modal Hapus Group Header -->
<div id="modal-hapus-data-master" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-danger">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h6 class="modal-title" id="modal-title-hapus-data-master">Hapus Data Master</h6>
			</div>
			<form id="form-hapus-data-master" action="<?=base_url("dashboard/setting/data-master/proses-hapus")?>" method="POST">
			<div class="modal-body">
				<h6 class="text-semibold" id="modal-subtitle-hapus-data-master">Apakah yakin ingin hapus Data Master?</h6>
				<p>Data Master yang dihapus akan hilang dari sistem dan tidak akan bisa dikembalikan. </p>
				<hr>
				<p>Jika ingin membatalkan permintaan, anda bisa mengklik tombol Batal.</p>
			</div>

			<input type="hidden" class="input-id-data-master" name="data_master_id">
			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">Batal</button>
				<button type="submit" class="btn btn-danger" id="btn-proses-hapus-data-master">Proses</button>
			</div>
			</form>
		</div>
	</div>
</div>