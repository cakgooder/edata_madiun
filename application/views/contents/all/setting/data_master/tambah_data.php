
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Form Tambah Data Master</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        	</ul>
    	</div>
	</div>
	<!-- PANEL BODY -->
	<div class="panel-body">
		<p><span class="text-danger">*</span> Wajib Diisi</p>

		<form class="form-horizontal" method="POST" action="<?=base_url("dashboard/setting/data-master/proses-tambah/")?>">
			<fieldset class="content-group">
				<legend class="text-bold">Detail Data Master</legend>
				<input type="hidden" name="urutan_kolom_key">

				<div class="form-group">
					<label class="control-label col-lg-2">Urusan</label>
					<div class="col-lg-10">
						<select name="urusan_kode" class="select-search" required="required">
							<option class="bg-grey-300" value="">Pilih Salah Satu</option>
<?php
			            	foreach($list_urusan as $row_urusan):
?>
			                <option value="<?=$row_urusan["urusan_kode"]?>"><?=$row_urusan["urusan_narasi"]?></option>
<?php
			            	endforeach;
?>
			            </select>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Nama Data Master <span class="text-danger">*</span></label>
					<div class="col-lg-10">
						<input type="text" name="data_master_nama" value="" class="form-control" required="required">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Ket Data Master </label>
					<div class="col-lg-10">
						<input type="text" name="data_master_ket" value="" class="form-control">
					</div>
				</div>

			</fieldset>
			<p>Drag and Drop Kolom Header untuk merubah urutan kolom</p>

			<div class="row" id="kolom-data">
				
			</div>

			<div class="form-group">
				<button class="btn bg-success-700" id="btn-data-tambah-kolom"><i class="icon-spinner spinner position-left" id="loading-tambah-kolom-data" style="display: none;"></i> Tambah Kolom</button>
			</div>

			<div class="form-group">
				<button type="submit" class="btn btn-primary">Simpan</button>
				<button type="button" class="btn btn-default" onclick="event.preventDefault();location.href='<?=base_url("dashboard/setting/data-master/")?>';">Kembali</button>
			</div>
		</form>

	</div>
	<!-- END PANEL BODY -->
</div>
<!-- /simple panel -->

<!-- Modal Hapus User -->
<div id="modal-hapus-kolom" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-danger">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h6 class="modal-title">Hapus Kolom</h6>
			</div>
			<input type="hidden" name="hapus_proses" value="">

			<div class="modal-body">
				<h6 class="text-semibold">Apakah yakin ingin menghapus Kolom ini dari Data Master ini?</h6>
				<p>Jika anda ingin menambahkan kolom kembali, tambahkan ulang melalui form.</p>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">Batal</button>
				<button type="button" class="btn btn-danger" id="btn-yakin-remove-kolom">Yakin</button>
			</div>
		</div>
	</div>
</div>