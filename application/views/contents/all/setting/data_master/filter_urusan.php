<!-- TABLE -->
<fieldset class="content-group">
	<legend class="text-bold">List Data Master</legend>
	<div class="table-responsive">
		<table class="table table-bordered">
			<thead>
				<tr class="border-solid">
					<th>#</th>
					<th>Narasi</th>
					<th>Keterangan</th>
					<th>File Excel</th>
					<th>Aksi</th>

				</tr>
			</thead>
			<tbody>
				<?php
				$i = 1;
				foreach ($list_data_master as $row_data_master):
				?>
					<tr class="row-data-master-<?=$row_data_master["data_master_nama"]?>">
						<td><?=$i?></td>
						<td><?=$row_data_master["data_master_nama"]?></td>
						<td><?=$row_data_master["data_master_ket"]?></td>
						<td><?=$row_data_master["data_master_file_excel"]?></td>
						<td>
							<div class="btn-group-vertical">
							<!-- Button Edit -->
							<button type="button" class="btn btn-primary btn-icon" data-popup="tooltip" title="Edit Data Master" data-placement="top" onclick="location.href='<?=base_url("dashboard/setting/data-master/edit/".encrypting_code($row_data_master["data_master_id"]))?>';"><i class="icon-wrench"></i></button>
							<!-- Button Hapus -->
							<button type="button" class="btn btn-danger btn-icon btn-hapus-data-master" data-popup="tooltip" title="Hapus Data Master" data-placement="top" data-id-data-master="<?=encrypting_code($row_data_master["data_master_id"])?>"><i class="icon-trash"></i></button>
						</div>
						</td>
					</tr>
				<?php
				$i++;
				endforeach;
				?>
			</tbody>
		</table>
	</div>
</fieldset>
<!-- END TABLE -->

<script type="text/javascript">
// Tooltip
$('[data-popup="tooltip"]').tooltip();
</script>