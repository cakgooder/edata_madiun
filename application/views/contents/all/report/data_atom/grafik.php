<!-- Simple panel -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Report Data Atom</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        	</ul>
    	</div>
	</div>

	<!-- PANEL BODY -->
	<div class="panel-body">

			<fieldset class="content-group">

				<div class="form-group">
					<label class="control-label col-lg-2">Data Atom</label>
					<div class="col-lg-4">
						<select name="data_atom_id_enc" class="select-search" id="select-data-atom" required="required">
							<option class="bg-grey-300" value="0">Pilih Salah Satu</option>
<?php
			            	foreach($daftar_data_atom as $row_atom):
?>
			                <option value="<?=encrypting_code($row_atom["data_atom_id"])?>">(<?=$row_atom["data_atom_id"]?>) <?=$row_atom["data_atom_narasi"]?></option>
<?php
			            	endforeach;
?>
			            </select>
					</div>
					<div class="col-lg-2">
						<button type="submit" class="btn btn-primary btn-tambah-grafik">Tambah Grafik</button>
					</div>
				</div>

			</fieldset>

			<fieldset class="content-group">
				<legend class="text-bold">List Data Atom Aktif</legend>
				<div class="form-group" id="body-list-data-atom">
					
				</div>

			</fieldset>

	</div>
	<!-- END PANEL BODY -->
</div>

<!-- Bar chart -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h6 class="panel-title text-semibold">Tampilan Grafik</h6>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<li><a data-action="reload"></a></li>
        		<li><a data-action="close"></a></li>
        	</ul>
    	</div>
	</div>

	<div class="panel-body hasil-refresh-grafik">
		<div class="chart-container">
			<div class="chart" id="c3-bar-chart"></div>
		</div>
	</div>
	<div class="panel-body loading-refresh-grafik" style="display:none">
		<i class="icon-spinner9 spinner"></i> Tunggu Sebentar
	</div>
</div>
<!-- /bar chart -->
<script type="text/javascript">
    
</script>