<!-- Simple panel -->
<div class="panel panel-flat">
	<!-- PANEL BODY -->
	<div class="panel-body" id="forms-target-left">
		<form class="form-horizontal form-refresh-filter" method="POST" action="<?=base_url("dashboard/master/data-master/proses-filter-urusan")?>">

			<fieldset class="content-group">

				<div class="form-group">
					<label class="control-label col-lg-2">Urusan</label>
					<div class="col-lg-10">
						<select name="urusan_kode" class="select-search" id="" required="required">
							<option class="bg-grey-300" value="">Pilih Salah Satu</option>
							<?php foreach ($list_urusan as $row_urusan): ?>
								<option value="<?=$row_urusan["urusan_kode"]?>"><?=$row_urusan["urusan_narasi"]?></option>
							<?php endforeach; ?>
			            </select>
					</div>
				</div>

				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-refresh-filter" data-to-refresh="refresh-filter">Refresh</button>
				</div>

			</fieldset>

		</form>

	</div>
	<!-- END PANEL BODY -->
</div>

<!-- Simple panel -->
<div class="panel panel-flat">
	<!-- PANEL BODY -->
	<div class="panel-body hasil-refresh-filter" id="">
	</div>
	<div class="panel-body loading-refresh-filter" id="" style="display:none">
		<i class="icon-spinner9 spinner"></i> Tunggu Sebentar
	</div>
	<!-- END PANEL BODY -->
</div>
<!-- /simple panel -->