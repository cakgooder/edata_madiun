<!-- TABLE SUB -->
<fieldset class="content-group">
	<legend class="text-bold">Daftar Data Master</legend>
	<p>Keterangan Warna Tahun :
		<?php for($i = $_tahun_mulai; $i <= $_tahun_berakhir; $i++): ?>
			<span class="label bg-<?=get_color_by_tahun($i)?>" data-popup="tooltip" data-placement="top" title="Data Tahun <?=$i?>">Data Tahun <?=$i?></span>
		<?php endfor; ?>
	</p>
	<div class="table-responsive">
		<table class="table table-bordered">
			<thead>
				<tr class="border-solid">
					<th>ID Data Master</th>
					<th>Nama Data Master</th>
				<?php for($i = $_tahun_mulai; $i <= $_tahun_berakhir; $i++): ?>
					<th class="bg-<?=get_color_by_tahun($i)?>"><?=$i?></th>
				<?php endfor; ?>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach($list_data_master as $row_data) :
				?>
				<tr id="row-data-master-<?=$row_data["data_master_id"]?>">
					<td><?=$row_data["data_master_id"]?></td>
					<td><?=$row_data["data_master_nama"]?></td>
					<?php
						for($i = $_tahun_mulai; $i <= $_tahun_berakhir; $i++):
							$color = "success-800";
					?>
					<td class="bg-<?=get_color_by_tahun($i)?>">
						<div class="btn-group-vertical">
							<!-- Button View -->
							<!-- <button type="button" class="btn bg-<?=$color?> btn-icon btn-lihat-data-master" data-popup="tooltip" title="Lihat Data Master" data-placement="top" data-id-data-master="<?=$row_data["data_master_id"]?>" data-id-data-master-enc="<?=encrypting_code($row_data["data_master_id"])?>" data-tahun-data="<?=$i?>"><i class="icon-folder-search"></i></button> -->

							<!-- Button Upload -->
							<!-- <button type="button" class="btn bg-success-300 btn-icon btn-upload-data-master" data-popup="tooltip" title="Upload Ulang Data Master" data-placement="top" data-id-data-master="<?=$row_data["data_master_id"]?>" data-id-data-master-enc="<?=encrypting_code($row_data["data_master_id"])?>" data-tahun-data="<?=$i?>"><i class="icon-upload"></i></button> -->
						</div>
					</td>
					<?php
						endfor;
					?>
				</tr>
				<?php
				endforeach;
				?>
			</tbody>
		</table>
	</div>
</fieldset>
<!-- END TABLE SUB -->

<script type="text/javascript">
// Tooltip
$('[data-popup="tooltip"]').tooltip();
</script>
