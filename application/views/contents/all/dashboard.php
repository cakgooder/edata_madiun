
<div class="row">

	<!-- Jumlah Data Master  -->
	<div class="col-lg-4">

		<div class="panel bg-blue-800">
			<div class="panel-body">
				<h3 class="no-margin"><button href="#" class="btn border-white text-white btn-flat btn-rounded btn-icon btn-xs valign-text-bottom" data-popup="tooltip" title="Jumlah Data Master"><i class="icon-books"></i></button> <?=$jml_data_master?></h3>
				Jumlah Data Master
			</div>
		</div>

	</div>

	<!-- Jumlah Data Atom -->
	<div class="col-lg-4">

		<div class="panel bg-danger-400">
			<div class="panel-body">
				<h3 class="no-margin"><button class="btn border-white text-white btn-flat btn-rounded btn-icon valign-text-bottom" data-popup="tooltip" title="Jumlah Data Atom"><i class="icon-book"></i></button>  <?=$jml_data_atom?></h3>
				Jumlah Data Atom
			</div>
		</div>

	</div>

</div>