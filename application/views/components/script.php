
	<!-- Theme JS files -->
	<script type="text/javascript" src="<?=base_url("assets/js/core/libraries/jquery_ui/interactions.min.js")?>"></script>
	<script type="text/javascript" src="<?=base_url("assets/js/core/libraries/jquery_ui/touch.min.js")?>"></script>
	<script type="text/javascript" src="<?=base_url("assets/js/plugins/tables/datatables/datatables.min.js")?>"></script>
	<script type="text/javascript" src="<?=base_url("assets/js/plugins/ui/moment/moment.min.js")?>"></script>
	<script type="text/javascript" src="<?=base_url("assets/js/plugins/forms/selects/select2.min.js")?>"></script>
	<script type="text/javascript" src="<?=base_url("assets/js/plugins/forms/styling/uniform.min.js")?>"></script>
	<script type="text/javascript" src="<?=base_url("assets/js/plugins/forms/styling/switch.min.js")?>"></script>
	<script type="text/javascript" src="<?=base_url("assets/js/plugins/notifications/pnotify.min.js")?>"></script>
	<script type="text/javascript" src="<?=base_url("assets/js/plugins/notifications/noty.min.js")?>"></script>
	<script type="text/javascript" src="<?=base_url("assets/js/plugins/notifications/jgrowl.min.js")?>"></script>

	<script type="text/javascript" src="<?=base_url("assets/js/plugins/media/fancybox.min.js")?>"></script>

	<script type="text/javascript" src="<?=base_url("assets/js/plugins/loaders/progressbar.min.js")?>"></script>

	<script type="text/javascript" src="<?=base_url("assets/js/plugins/ui/dragula.min.js");?>"></script>

	<script type="text/javascript" src="<?=base_url("assets/js/plugins/pagination/bootpag.min.js")?>"></script>
	<script type="text/javascript" src="<?=base_url("assets/js/plugins/pagination/bs_pagination.min.js")?>"></script>
	<script type="text/javascript" src="<?=base_url("assets/js/plugins/pagination/datepaginator.min.js")?>"></script>

	<script type="text/javascript" src="<?=base_url("assets/js/plugins/visualization/d3/d3.min.js")?>"></script>
	<script type="text/javascript" src="<?=base_url("assets/js/plugins/visualization/c3/c3.min.js")?>"></script>


	<script type="text/javascript" src="<?=base_url("assets/js/core/app.js")?>"></script>

	<script type="text/javascript" src="<?=base_url("assets/js/pages/extension_dnd.js");?>"></script>

	<script type="text/javascript" src="<?=base_url("assets/js/pages/components_pagination.js")?>"></script>

	<script type="text/javascript" src="<?=base_url("assets/js/pages/components_loaders.js")?>"></script>

	<script type="text/javascript" src="<?=base_url("assets/js/pages/gallery.js")?>"></script>

	<script type="text/javascript" src="<?=base_url("assets/js/pages/form_inputs.js")?>"></script>
	<script type="text/javascript" src="<?=base_url("assets/js/pages/datatables_basic.js")?>"></script>

	<script type="text/javascript" src="<?=base_url("assets/js/plugins/forms/submit/jquery.form.js")?>"></script>
	<script type="text/javascript" src="<?=base_url("assets/js/pages/form_select2.js")?>"></script>
	<!-- /theme JS files -->

	<!-- MY THEME -->
	<script type="text/javascript">

		var BASE_URL = "<?=base_url()?>";
		var INDEX_KOLOM_DATA = <?php if(isset($index_kolom)) { echo $index_kolom; } else { echo 1; }?>;
		var TAHUN_DATA = [<?php for($i = $_tahun_mulai; $i <= $_tahun_berakhir; $i++) { echo $i.", "; } ?>];

		/* Script from Controller */
		<?php
		echo $_notif_script;
		?>

	</script>
	<script type="text/javascript" src="<?=base_url("assets/js/core/custom/gooder.js")?>"></script>
	<script type="text/javascript" src="<?=base_url("assets/js/core/custom/button_clicked.js")?>"></script>
	<script type="text/javascript" src="<?=base_url("assets/js/core/custom/form_submitted.js")?>"></script>
	<!-- /MY THEME -->
