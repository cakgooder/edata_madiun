
<div class="navbar navbar-inverse navbar-transparent navbar-fixed-top">
	<div class="navbar-header">
		<a class="navbar-brand" href="<?=base_url()?>"><img src="<?=base_url("assets/images/white.png")?>"></span></a>

		<ul class="nav navbar-nav pull-right visible-xs-block">
			<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-grid3"></i></a></li>
		</ul>
	</div>

	<div class="navbar-collapse collapse" id="navbar-mobile">

		<div class="navbar-right">
			<ul class="nav navbar-nav">
				<li class="dropdown dropdown-user">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<img src="<?=base_url("assets/images/favicon-96x96.png")?>" alt="">
						<span><?=$this->session->userdata("user_nama")?></span>
						<i class="caret"></i>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="<?=base_url("setting")?>"><i class="icon-cog"></i> Setting Akun</a></li>
						<li><a href="<?=base_url("logout")?>"><i class="icon-switch2"></i> Logout</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</div>
