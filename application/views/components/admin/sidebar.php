
<div class="sidebar sidebar-main">
	<div class="sidebar-content">

		<!-- User menu -->
		<div class="sidebar-user">
			<div class="category-content">
				<div class="media">
					<a href="#" class="media-left"><img src="<?=base_url("assets/images/favicon-96x96.png")?>" class="img-circle img-sm" alt=""></a>
					<div class="media-body">
						<span class="media-heading text-semibold"><?=$this->session->userdata("user_nama")?></span>
						<div class="text-size-mini text-muted">
							<i class="icon-user text-size-small"></i> &nbsp;<?=$_daftar_user_level[$this->session->userdata("user_level")]["user_level_nama"]?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /user menu -->


		<!-- Main navigation -->
		<div class="sidebar-category sidebar-category-visible">
			<div class="category-content no-padding">
				<ul class="navigation navigation-main navigation-accordion">

					<!-- Main -->
					<li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
					<li class="<?php if($_sidebar_focus == "dashboard"){ echo "active"; }?>"><a href="<?=base_url()?>"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
					<li>
						<a href="#"><i class="icon-database"></i> <span>Master</span></a>
						<ul>
							<li class="<?php if($_sidebar_focus == "master-data-master"){ echo "active"; } ?>"><a href="<?=base_url('dashboard/master/data-master/')?>">Data Master</a></li>
						</ul>
					</li>
					<li>
						<a href="#"><i class="icon-copy"></i> <span>Report</span></a>
						<ul>
							<li>
								<a href="#"><span>Data Atom</span></a>
								<ul>
									<li class="<?php if($_sidebar_focus == "report-grafik-data-atom"){ echo "active"; }?>"><a href="<?=base_url("dashboard/report/data-atom/grafik")?>">Grafik</a></li>
								</ul>
							</li>
						</ul>
					</li>
					<li>
						<a href="#"><i class="icon-cog"></i> <span>Setting</span></a>
						<ul>
							<li class="navigation-header"><span>Edata</span></li>
							<li class="<?php if($_sidebar_focus == "setting-data-master"){ echo "active"; }?>"><a href="<?=base_url("dashboard/setting/data-master")?>">Data Master</a></li>
							<li class="<?php if($_sidebar_focus == "setting-data-atom"){ echo "active"; }?>"><a href="<?=base_url("dashboard/setting/data-atom")?>">Data Atom</a></li>

							<li class="navigation-header"><span>SIM</span></li>
							<li class="<?php if($_sidebar_focus == "setting-user"){ echo "active"; } ?>"><a href="<?=base_url("dashboard/setting/user")?>">User</a></li>
							<li class="<?php if($_sidebar_focus == "setting-tahun-data"){ echo "active"; } ?>"><a href="<?=base_url("dashboard/setting/tahun-data")?>">Tahun Data</a></li>
							<!-- <li class="<?php if($_sidebar_focus == "setting-api-key"){ echo "active"; } ?>"><a href="#">API Key</a></li> -->
							<!-- <li class="<?php if($_sidebar_focus == "setting-milestone"){ echo "active"; } ?>"><a href="#">Milestone</a></li> -->

						</ul>
					</li>
					<!-- /main -->
				</ul>
			</div>
		</div>
		<!-- /main navigation -->

	</div>
</div>
