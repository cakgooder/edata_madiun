<div class="navbar-collapse collapse" id="navbar-second-toggle">
	<ul class="nav navbar-nav navbar-nav-material">
		<li class="<?php if($_sidebar_focus == "dashboard"){ echo "active"; } ?>"><a href="<?php echo base_url("dashboard"); ?>">Dashboard</a></li>

		<li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
				Master <span class="caret"></span>
			</a>

			<ul class="dropdown-menu width-250">
				<li class="<?php if($_sidebar_focus == "master-data-master"){ echo "active"; } ?>"><a href="<?=base_url('dashboard/master/data-master/')?>">Data Master</a></li>
				<li class="<?php if($_sidebar_focus == "master-data-atom"){ echo "active"; } ?>"><a href="#">Data Atom</a></li>
			</ul>
		</li>

		<li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
				Upload <span class="caret"></span>
			</a>

			<ul class="dropdown-menu width-250">
				<li class="<?php if($_sidebar_focus == "upload-data-master"){ echo "active"; } ?>"><a href="#">Data Master</a></li>
			</ul>
		</li>

		<li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
				Cron Job<span class="caret"></span>
			</a>

			<ul class="dropdown-menu width-250">
				<li class="dropdown-header highlight">Data Master</li>
				<li class="<?php if($_sidebar_focus == "cron-data-master-write"){ echo "active"; } ?>"><a href="#">Data Master Write Excel</a></li>
				<li class="<?php if($_sidebar_focus == "cron-data-master-read"){ echo "active"; } ?>"><a href="#">Data Master Read Excel</a></li>
				<li class="dropdown-header highlight">Rawdata</li>
				<li class="<?php if($_sidebar_focus == "cron-realisasi-data-atom"){ echo "active"; } ?>"><a href="#">Realisasi Data Atom</a></li>
			</ul>
		</li>

		<li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
				Report <span class="caret"></span>
			</a>

			<ul class="dropdown-menu width-250">
				<li class="dropdown-header highlight">Data Atom</li>
				<li class="<?php if($_sidebar_focus == "report-realisasi-data-atom"){ echo "active"; } ?>"><a href="#">Nilai Realisasi Data Atom</a></li>
			</ul>
		</li>

		<li class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
				Setting <span class="caret"></span>
			</a>

			<ul class="dropdown-menu width-250">
				<li class="dropdown-header highlight">SIM</li>
				<li class="<?php if($_sidebar_focus == "setting-user"){ echo "active"; } ?>"><a href="#">User</a></li>
				<li class="<?php if($_sidebar_focus == "setting-api-key"){ echo "active"; } ?>"><a href="#">API Key</a></li>
				<li class="<?php if($_sidebar_focus == "setting-active-periode"){ echo "active"; } ?>"><a href="#">Active Periode</a></li>
			</ul>
		</li>

	</ul>
</div>
