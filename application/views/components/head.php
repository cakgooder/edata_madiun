
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- META CACHE -->
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
	<meta http-equiv="pragma" content="no-cache" />

	<!-- TITLE -->
	<title><?=$_head_title?> - <?=$_apps_title?></title>

	<!-- ICON -->
	<link rel="icon" type="image/png" sizes="16x16" href="<?=base_url("assets/images/favicon-96x96.png")?>">

	<!-- Global stylesheets -->
	<link href="<?=base_url("assets/css/roboto-googleapis.css")?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url("assets/css/icons/icomoon/styles.css")?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url("assets/css/bootstrap.css")?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url("assets/css/core.css")?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url("assets/css/components.css")?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url("assets/css/colors.css")?>" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?=base_url("assets/js/plugins/loaders/pace.min.js")?>"></script>
	<script type="text/javascript" src="<?=base_url("assets/js/core/libraries/jquery.min.js")?>"></script>
	<script type="text/javascript" src="<?=base_url("assets/js/core/libraries/bootstrap.min.js")?>"></script>
	<script type="text/javascript" src="<?=base_url("assets/js/plugins/loaders/blockui.min.js")?>"></script>
	<script type="text/javascript" src="<?=base_url("assets/js/plugins/clipboard/clipboard.min.js")?>"></script>
	<!-- /core JS files -->

</head>