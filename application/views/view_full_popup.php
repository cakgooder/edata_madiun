<!DOCTYPE html>
<html lang="en">

<!-- HEAD -->
<?php
echo $__head_page;
?>
<!-- END HEAD -->

<body>
	<!-- Page container -->
	<div class="page-container page-container-full-popup">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					<?php
					echo $__content_page;
					?>

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

	<!-- LOAD SCRIPT THEME -->
	<?php
	echo $__script_page;
	?>
	
	<!-- /LOAD SCRIPT THEME -->

</body>
</html>
