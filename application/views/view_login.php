
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Login - <?=$_apps_title?></title>

	<!-- ICON -->
	<link rel="icon" type="image/png" sizes="16x16" href="<?=base_url("assets/images/favicon-96x96.png")?>">

	<!-- Global stylesheets -->
	<link href="<?=base_url("assets/css/roboto-googleapis.css")?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url("assets/css/icons/icomoon/styles.css")?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url("assets/css/bootstrap.css")?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url("assets/css/core.css")?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url("assets/css/components.css")?>" rel="stylesheet" type="text/css">
	<link href="<?=base_url("assets/css/colors.css")?>" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?=base_url("assets/js/plugins/loaders/pace.min.js")?>"></script>
	<script type="text/javascript" src="<?=base_url("assets/js/core/libraries/jquery.min.js")?>"></script>
	<script type="text/javascript" src="<?=base_url("assets/js/core/libraries/bootstrap.min.js")?>"></script>
	<script type="text/javascript" src="<?=base_url("assets/js/plugins/loaders/blockui.min.js")?>"></script>
	<script type="text/javascript" src="<?=base_url("assets/js/plugins/clipboard/clipboard.min.js")?>"></script>
	<!-- /core JS files -->

</head>

<body class="login-container">

	<!-- Main navbar -->
	<div class="navbar navbar-inverse bg-indigo">
		<div class="navbar-header">
			<a class="navbar-brand" href="<?=base_url()?>"><img src="<?=base_url("assets/images/white.png")?>" style="height: 20px"></a>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					<!-- Simple login form -->
					<form id="form-login" action="<?=base_url("login/proses")?>" method="POST" autocomplete="off">
						<div class="panel panel-body login-form">
							<div class="text-center">
								<div class=""><img src="<?=base_url("assets/images/edata8.png")?>" width="150px"></div>
								<h5 class="content-group">Silahkan Login <small class="display-block">Masukkan username dan password</small></h5>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								<input type="text" class="form-control" name="username" autofocus placeholder="Username" required="required" oninvalid="this.setCustomValidity('Username tidak boleh kosong')" oninput="setCustomValidity('')">
								<div class="form-control-feedback">
									<i class="icon-user text-muted"></i>
								</div>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								<input type="password" class="form-control" name="password" placeholder="Password" required="required" oninvalid="this.setCustomValidity('Password tidak boleh kosong')" oninput="setCustomValidity('')">
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
							</div>

							<div class="form-group">
								<button type="submit" id="button-submit-login" class="btn bg-pink-400 btn-block">Login <i class="icon-circle-right2 position-right"></i></button>
							</div>

						</div>
					</form>
					<!-- /simple login form -->

					<!-- Footer -->
					<div class="footer text-muted text-center">
						&copy; <?=date("Y")?>. <a href="<?=base_url()?>"><?=$_apps_system_name?></a> by <a href="<?=$_apps_company_site?>"><?=$_apps_company_name?> - <?=$_apps_company_city?></a>
					</div>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

	<!-- SCRIPT -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?=base_url("assets/js/plugins/notifications/pnotify.min.js")?>"></script>
	<script type="text/javascript" src="<?=base_url("assets/js/plugins/notifications/noty.min.js")?>"></script>
	<script type="text/javascript" src="<?=base_url("assets/js/plugins/notifications/jgrowl.min.js")?>"></script>
	<script type="text/javascript" src="<?=base_url("assets/js/plugins/forms/validation/validate.min.js")?>"></script>
	<script type="text/javascript" src="<?=base_url("assets/js/plugins/forms/styling/uniform.min.js")?>"></script>
	<script type="text/javascript" src="<?=base_url("assets/js/core/app.js")?>"></script>
	<script type="text/javascript" src="<?=base_url("assets/js/pages/login_validation.js")?>"></script>

	<script type="text/javascript" src="<?=base_url("assets/js/plugins/ui/ripple.min.js")?>"></script>
	<!-- /theme JS files -->

	<!-- My JS -->
	<script type="text/javascript" src="<?=base_url("assets/js/core/custom/gooder.js")?>"></script>
	<script type="text/javascript">
		
		/* Script from Controller */
		<?php
			echo $_notif_script;
		?>


	</script>
	<!-- /My JS -->

	<!-- /SCRIPT -->

</body>
</html>
