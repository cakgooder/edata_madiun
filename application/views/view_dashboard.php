<!DOCTYPE html>
<html lang="en">

<!-- HEAD -->
<?php
echo $__head_page;
?>
<!-- END HEAD -->

<body class="navbar-top">

	<!-- Main navbar -->
	<?php
	echo $__navbar_page;
	?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php
			echo $__sidebar_page;
			?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<?php
				echo $__header_page;
				?>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<?php
					echo $__content_page;
					?>


					<!-- Footer -->
					<div class="footer text-muted">
						&copy; <?=date("Y")?>. <a href="<?=base_url()?>"><?=$_apps_system_name?></a> by <a href="<?=$_apps_company_site?>"><?=$_apps_company_name?> - <?=$_apps_company_city?></a>
					</div>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

	<!-- LOAD SCRIPT THEME -->
	<?php
	echo $__script_page;
	?>
	
	<!-- /LOAD SCRIPT THEME -->

</body>
</html>
