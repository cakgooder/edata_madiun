<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_sample_kolom extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		/* Insert Data sample */
		$data_insert = [];
		$data_insert[0]["data_master_id"] = "1";
		$data_insert[0]["kolom_ke"] = "1";
		$data_insert[0]["kolom_nama"] = "NIS";
		$data_insert[0]["kolom_kode"] = "nis";
		$data_insert[0]["kolom_type_data"] = "char";

		$data_insert[1]["data_master_id"] = "1";
		$data_insert[1]["kolom_ke"] = "2";
		$data_insert[1]["kolom_nama"] = "Kode Sekolah";
		$data_insert[1]["kolom_kode"] = "kode_sekolah";
		$data_insert[1]["kolom_type_data"] = "char";

		$data_insert[2]["data_master_id"] = "1";
		$data_insert[2]["kolom_ke"] = "3";
		$data_insert[2]["kolom_nama"] = "Nama Siswa";
		$data_insert[2]["kolom_kode"] = "nama_siswa";
		$data_insert[2]["kolom_type_data"] = "char";

		$data_insert[3]["data_master_id"] = "1";
		$data_insert[3]["kolom_ke"] = "4";
		$data_insert[3]["kolom_nama"] = "Kelas";
		$data_insert[3]["kolom_kode"] = "kelas";
		$data_insert[3]["kolom_type_data"] = "char";

		$data_insert[4]["data_master_id"] = "2";
		$data_insert[4]["kolom_ke"] = "1";
		$data_insert[4]["kolom_nama"] = "NIS";
		$data_insert[4]["kolom_kode"] = "nis";
		$data_insert[4]["kolom_type_data"] = "char";

		$data_insert[5]["data_master_id"] = "2";
		$data_insert[5]["kolom_ke"] = "2";
		$data_insert[5]["kolom_nama"] = "Kode Sekolah";
		$data_insert[5]["kolom_kode"] = "kode_sekolah";
		$data_insert[5]["kolom_type_data"] = "char";

		$data_insert[6]["data_master_id"] = "2";
		$data_insert[6]["kolom_ke"] = "3";
		$data_insert[6]["kolom_nama"] = "Nama Siswa";
		$data_insert[6]["kolom_kode"] = "nama_siswa";
		$data_insert[6]["kolom_type_data"] = "char";

		$data_insert[7]["data_master_id"] = "2";
		$data_insert[7]["kolom_ke"] = "4";
		$data_insert[7]["kolom_nama"] = "Kelas";
		$data_insert[7]["kolom_kode"] = "kelas";
		$data_insert[7]["kolom_type_data"] = "char";
		$this->db->insert_batch("master_kolom_data_master", $data_insert);		
	}

	public function down() {
		/* Hapus data */
		$this->db->truncate("master_kolom_data_master");
	}

}

/* End of file 20200411171500_add_sample_kolom.php */
/* Location: ./application/migrations/20200411171500_add_sample_kolom.php */