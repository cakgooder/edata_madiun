<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_kolom_data_master extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$this->dbforge->add_field(array(
			'data_master_id' => array(
				'type' => 'INT',
			),
			'kolom_ke' => array(
				'type' => 'INT',
			),
			'kolom_nama' => array(
				'type' => 'VARCHAR',
				'constraint' => '250',
			),
			'kolom_kode' => array(
				'type' => 'VARCHAR',
				'constraint' => '250',
			),
			'kolom_type_data' => array(
				'type' => 'VARCHAR',
				'constraint' => '10',
			),
			'kolom_ket' => array(
				'type' => 'VARCHAR',
				'constraint' => '500',
				'null' => TRUE,
			),
			'kolom_boolean' => array(
				'type' => 'TEXT',
				'null' => TRUE,
			),
		));
		$this->dbforge->add_key(array("data_master_id", "kolom_ke"), TRUE);
		$this->dbforge->create_table('master_kolom_data_master');
	}

	public function down() {
		$this->dbforge->drop_table('master_kolom_data_master');
	}

}

/* End of file 20200331164900_add_kolom_data_master.php */
/* Location: ./application/migrations/20200331164900_add_kolom_data_master.php */