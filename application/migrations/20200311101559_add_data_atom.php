<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_data_atom extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$this->dbforge->add_field(array(
			'data_atom_id' => array(
				'type' => 'INT',
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'data_master_id' => array(
				'type' => 'INT',
			),
			'data_atom_narasi' => array(
				'type' => 'VARCHAR',
				'constraint' => '900',
			),
			'data_atom_ket' => array(
				'type' => 'TEXT',
				'null' => TRUE,
			),
			'data_atom_satuan' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
				'null' => TRUE,
			),
		));
		$this->dbforge->add_key('data_atom_id', TRUE);
		$this->dbforge->create_table('master_data_atom_list');
	}

	public function down() {
		$this->dbforge->drop_table('master_data_atom_list');
	}

}

/* End of file 20200311101559_add_data_atom.php */
/* Location: ./application/migrations/20200311101559_add_data_atom.php */