<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_user extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$this->dbforge->add_field(array(
			'user_id' => array(
				'type' => 'INT',
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'username' => array(
				'type' => 'VARCHAR',
				'constraint' => '30',
			),
			'password' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'user_nama' => array(
				'type' => 'VARCHAR',
				'constraint' => '255',
			),
			'user_level' => array(
				'type' => 'VARCHAR',
				'constraint' => '10',
			),
			'locked' => array(
				'type' => 'VARCHAR',
				'constraint' => '10',
				'default' => 'unlocked',
			),
			'last_login' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,
			),
		));
		$this->dbforge->add_key('user_id', TRUE);
		$this->dbforge->create_table('sim_user_list');
	}

	public function down() {
		$this->dbforge->drop_table('sim_user_list');
	}

}

/* End of file 20200309080259_add_user.php */
/* Location: ./application/migrations/20200309080259_add_user.php */