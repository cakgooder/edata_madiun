<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_urusan extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$this->dbforge->add_field(array(
			'urusan_kode' => array(
				'type' => 'VARCHAR',
				'constraint' => '50',
			),
			'urusan_narasi' => array(
				'type' => 'VARCHAR',
				'constraint' => '900',
			),
			'digit' => array(
				'type' => 'INT',
				'null' => TRUE,
			),
			'inisial' => array(
				'type' => 'INT',
				'null' => TRUE,
			),
		));
		$this->dbforge->add_key('urusan_kode', TRUE);
		$this->dbforge->create_table('master_urusan_list');
	}

	public function down() {
		$this->dbforge->drop_table('master_urusan_list');
	}

}

/* End of file 20200311143159_add_urusan.php */
/* Location: ./application/migrations/20200311143159_add_urusan.php */