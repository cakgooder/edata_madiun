<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_user_admin extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		/* Insert Data Admin user */
		$data_insert[0]["username"] = "admin";
		$data_insert[0]["password"] = do_hash('admin', 'md5');
		$data_insert[0]["user_nama"] = "Admin";
		$data_insert[0]["user_level"] = "admin";
		$this->db->insert_batch("sim_user_list", $data_insert);

		/* User Level Admin */
		$data_insert_level[0]["user_level"] = "admin";
		$data_insert_level[0]["user_level_nama"] = "Admin";
		$this->db->insert_batch("sim_user_level_list", $data_insert_level);
	}

	public function down() {
		/* Hapus data di sim user list */
		$this->db->truncate("sim_user_list");

		/* Hapus data di sim user list */
		$this->db->truncate("sim_user_level_list");
	}

}

/* End of file 20200331110700_add_user_admin.php */
/* Location: ./application/migrations/20200331110700_add_user_admin.php */