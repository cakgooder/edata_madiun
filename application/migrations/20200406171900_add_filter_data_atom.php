<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_filter_data_atom extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$this->dbforge->add_field(array(
			'filter_id' => array(
				'type' => 'INT',
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'data_atom_id' => array(
				'type' => 'INT',
			),
			'filter_kolom_ke' => array(
				'type' => 'INT',
			),
			'filter_operator' => array(
				'type' => 'VARCHAR',
				'constraint' => '10',
			),
			'filter_value' => array(
				'type' => 'VARCHAR',
				'constraint' => '500',
				'null' => TRUE,
			),
		));
		$this->dbforge->add_key('filter_id', TRUE);
		$this->dbforge->create_table('master_data_atom_filter_list');
	}

	public function down() {
		$this->dbforge->drop_table('master_data_atom_filter_list');
	}

}

/* End of file 20200406171900_add_filter_data_atom.php */
/* Location: ./application/migrations/20200406171900_add_filter_data_atom.php */