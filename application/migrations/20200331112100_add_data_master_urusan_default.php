<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_data_master_urusan_default extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		/* Insert Data urusan */
		$data_insert[0]["urusan_kode"] = "pendidikan";
		$data_insert[0]["urusan_narasi"] = "Pendidikan";
		$data_insert[1]["urusan_kode"] = "kesehatan";
		$data_insert[1]["urusan_narasi"] = "Kesehatan";
		$this->db->insert_batch("master_urusan_list", $data_insert);

		/* insert data master */
		$data_insert = [];
		$data_insert[0]["urusan_kode"] = "pendidikan";
		$data_insert[0]["data_master_nama"] = "Siswa Sekolah SD";
		$data_insert[0]["data_master_ket"] = "Data seluruh Siswa Sekolah SD";
		$data_insert[1]["urusan_kode"] = "pendidikan";
		$data_insert[1]["data_master_nama"] = "Siswa Sekolah SMP";
		$data_insert[1]["data_master_ket"] = "Data seluruh Siswa Sekolah SMP";
		$data_insert[2]["urusan_kode"] = "kesehatan";
		$data_insert[2]["data_master_nama"] = "Rumah Sakit";
		$data_insert[2]["data_master_ket"] = "Data seluruh Rumah Sakit";
		$data_insert[3]["urusan_kode"] = "kesehatan";
		$data_insert[3]["data_master_nama"] = "Puskesmas";
		$data_insert[3]["data_master_ket"] = "Data seluruh Puskesmas";
		$this->db->insert_batch("master_data_master_list", $data_insert);
	}

	public function down() {
		/* Hapus data di master data */
		$this->db->truncate("master_data_master_list");

		/* Hapus data di urusan */
		$this->db->truncate("master_urusan_list");
	}

}

/* End of file 20200331112100_add_data_master_urusan_default.php */
/* Location: ./application/migrations/20200331112100_add_data_master_urusan_default.php */