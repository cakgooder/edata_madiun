<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_unit_kerja extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$this->dbforge->add_field(array(
			'unit_id' => array(
				'type' => 'VARCHAR',
				'constraint' => '10',
			),
			'unit_nama' => array(
				'type' => 'VARCHAR',
				'constraint' => '500',
			)
		));
		$this->dbforge->add_key('unit_id', TRUE);
		$this->dbforge->create_table('master_unit_kerja_list');
	}

	public function down() {
		$this->dbforge->drop_table('master_unit_kerja_list');
	}

}

/* End of file 20200312084159_add_unit_kerja.php */
/* Location: ./application/migrations/20200312084159_add_unit_kerja.php */