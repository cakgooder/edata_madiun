<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_column_data_atom extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$fields = array(
			'data_atom_kolom_realisasi_awal' => array(
				'type' => 'INT',
				'null' => TRUE,
			),
			'data_atom_kolom_realisasi_akhir' => array(
				'type' => 'INT',
				'null' => TRUE,
			),
			'data_atom_jenis_realisasi' => array(
				'type' => 'VARCHAR',
				'constraint' => '20',
				'null' => TRUE,
			),
			'data_atom_status' => array(
				'type' => 'VARCHAR',
				'constraint' => '10',
				'default' => 'active'
			),
		);
		$this->dbforge->add_column('master_data_atom_list', $fields);
	}

	public function down() {
		$this->dbforge->drop_column('master_data_atom_list', 'data_atom_kolom_realisasi_awal');
		$this->dbforge->drop_column('master_data_atom_list', 'data_atom_kolom_realisasi_akhir');
		$this->dbforge->drop_column('master_data_atom_list', 'data_atom_jenis_realisasi');
	}

}

/* End of file 20200406170100_add_column_data_atom.php */
/* Location: ./application/migrations/20200406170100_add_column_data_atom.php */