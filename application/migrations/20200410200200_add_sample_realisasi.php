<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_sample_realisasi extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		/* Insert Data sample */
		$data_insert = [];
		$data_insert[0]["data_master_id"] = "1";
		$data_insert[0]["data_atom_narasi"] = "Jumlah Siswa Sekolah SD";
		$data_insert[0]["data_atom_satuan"] = "orang";
		$data_insert[1]["data_master_id"] = "2";
		$data_insert[1]["data_atom_narasi"] = "Jumlah Siswa Sekolah SMP";
		$data_insert[1]["data_atom_satuan"] = "orang";
		$this->db->insert_batch("master_data_atom_list", $data_insert);

		/* Insert Data sample */
		$data_insert = [];
		$data_insert[0]["data_atom_id"] = "1";
		$data_insert[0]["realisasi_tahun_data"] = "2019";
		$data_insert[0]["realisasi_nilai"] = "25";
		$data_insert[1]["data_atom_id"] = "1";
		$data_insert[1]["realisasi_tahun_data"] = "2020";
		$data_insert[1]["realisasi_nilai"] = "100";
		$data_insert[2]["data_atom_id"] = "1";
		$data_insert[2]["realisasi_tahun_data"] = "2021";
		$data_insert[2]["realisasi_nilai"] = "50";
		$data_insert[3]["data_atom_id"] = "1";
		$data_insert[3]["realisasi_tahun_data"] = "2022";
		$data_insert[3]["realisasi_nilai"] = "75";
		$data_insert[4]["data_atom_id"] = "1";
		$data_insert[4]["realisasi_tahun_data"] = "2023";
		$data_insert[4]["realisasi_nilai"] = "100";
		$data_insert[5]["data_atom_id"] = "1";
		$data_insert[5]["realisasi_tahun_data"] = "2024";
		$data_insert[5]["realisasi_nilai"] = "125";
		$data_insert[6]["data_atom_id"] = "1";
		$data_insert[6]["realisasi_tahun_data"] = "2025";
		$data_insert[6]["realisasi_nilai"] = "100";

		$data_insert[7]["data_atom_id"] = "2";
		$data_insert[7]["realisasi_tahun_data"] = "2019";
		$data_insert[7]["realisasi_nilai"] = "100";
		$data_insert[8]["data_atom_id"] = "2";
		$data_insert[8]["realisasi_tahun_data"] = "2020";
		$data_insert[8]["realisasi_nilai"] = "50";
		$data_insert[9]["data_atom_id"] = "2";
		$data_insert[9]["realisasi_tahun_data"] = "2021";
		$data_insert[9]["realisasi_nilai"] = "25";
		$data_insert[10]["data_atom_id"] = "2";
		$data_insert[10]["realisasi_tahun_data"] = "2022";
		$data_insert[10]["realisasi_nilai"] = "25";
		$data_insert[11]["data_atom_id"] = "2";
		$data_insert[11]["realisasi_tahun_data"] = "2023";
		$data_insert[11]["realisasi_nilai"] = "75";
		$data_insert[12]["data_atom_id"] = "2";
		$data_insert[12]["realisasi_tahun_data"] = "2024";
		$data_insert[12]["realisasi_nilai"] = "100";
		$data_insert[13]["data_atom_id"] = "2";
		$data_insert[13]["realisasi_tahun_data"] = "2025";
		$data_insert[13]["realisasi_nilai"] = "125";
		$this->db->insert_batch("realisasi_data_atom", $data_insert);
	}

	public function down() {
		/* Hapus data sample */
		$this->db->truncate("master_data_atom_list");

		/* Hapus data sample */
		$this->db->truncate("realisasi_data_atom");
	}

}

/* End of file 20200410200200_add_sample_realisasi.php */
/* Location: ./application/migrations/20200410200200_add_sample_realisasi.php */