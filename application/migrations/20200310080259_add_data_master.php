<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_data_master extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$this->dbforge->add_field(array(
			'data_master_id' => array(
				'type' => 'INT',
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'urusan_kode' => array(
				'type' => 'VARCHAR',
				'constraint' => '50',
			),
			'data_master_nama' => array(
				'type' => 'VARCHAR',
				'constraint' => '200',
			),
			'data_master_ket' => array(
				'type' => 'TEXT',
				'null' => TRUE,
			),
			'data_master_file_excel' => array(
				'type' => 'VARCHAR',
				'constraint' => '500',
				'null' => TRUE,
			),
			'data_master_sesuai_excel' => array(
				'type' => 'VARCHAR',
				'constraint' => '10',
				'default' => 'queue'
			),
			'data_master_status' => array(
				'type' => 'VARCHAR',
				'constraint' => '10',
				'default' => 'active'
			),
		));
		$this->dbforge->add_key('data_master_id', TRUE);
		$this->dbforge->create_table('master_data_master_list');
	}

	public function down() {
		$this->dbforge->drop_table('master_data_master_list');
	}

}

/* End of file 20200310080259_add_data_master.php.php */
/* Location: ./application/migrations/20200310080259_add_data_master.php.php */