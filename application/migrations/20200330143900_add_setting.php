<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_setting extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$this->dbforge->add_field(array(
			'setting_kode' => array(
				'type' => 'VARCHAR',
				'constraint' => '50',
			),
			'setting_value' => array(
				'type' => 'TEXT',
			),
			'setting_last_change' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,
			),
		));
		$this->dbforge->add_key('setting_kode', TRUE);
		$this->dbforge->create_table('sim_setting');

		/* Insert Data */
		$data_insert[0]["setting_kode"] = "tahun_mulai";
		$data_insert[0]["setting_value"] = "2019";
		$data_insert[1]["setting_kode"] = "tahun_berakhir";
		$data_insert[1]["setting_value"] = "2025";
		$this->db->insert_batch("sim_setting", $data_insert);
	}

	public function down() {
		$this->dbforge->drop_table('sim_setting');
	}

}

/* End of file 20200330143900_add_setting.php */
/* Location: ./application/migrations/20200330143900_add_setting.php */