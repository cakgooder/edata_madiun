<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_realisasi_data_atom extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$this->dbforge->add_field(array(
			'realisasi_id' => array(
				'type' => 'INT',
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'data_atom_id' => array(
				'type' => 'INT',
			),
			'realisasi_tahun_data' => array(
				'type' => 'INT',
			),
			'realisasi_nilai' => array(
				'type' => 'FLOAT',
				'null' => TRUE,
			),
			'last_modified' => array(
				'type' => 'TIMESTAMP',
				'null' => TRUE,
			),
		));
		$this->dbforge->add_key('realisasi_id', TRUE);
		$this->dbforge->create_table('realisasi_data_atom');
	}

	public function down() {
		$this->dbforge->drop_table('realisasi_data_atom');
	}

}

/* End of file 20200406101000_add_realisasi_data_atom.php */
/* Location: ./application/migrations/20200406101000_add_realisasi_data_atom.php */