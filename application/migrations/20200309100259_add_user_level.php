<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_user_level extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$this->dbforge->add_field(array(
			'user_level' => array(
				'type' => 'VARCHAR',
				'constraint' => '10',
			),
			'user_level_nama' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
		));
		$this->dbforge->add_key('user_level', TRUE);
		$this->dbforge->create_table('sim_user_level_list');
	}

	public function down() {
		$this->dbforge->drop_table('sim_user_level_list');
	}

}

/* End of file 20200309100259_add_user_level.php */
/* Location: ./application/migrations/20200309100259_add_user_level.php */