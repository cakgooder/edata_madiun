<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_urusan_list extends MY_Model {

	/* Config dasar yang diturunkan ke seluruh model yang mengextend */
	protected $_table_name = 'master_urusan_list';
	protected $_primary_key = 'urusan_kode';
	protected $_group_by = '';
	protected $_order_by = '';
	protected $_sequence_name = 'master_data_master_list_data_master_id_seq';

	/* Fungsi Construct (wajib ada) */
	public function __construct()
	{
		parent::__construct();
		// $this->db->reset_query();
	}


}

/* End of file Master_urusan_list.php */
/* Location: ./application/models/Master_urusan_list.php */