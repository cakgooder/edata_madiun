<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Realisasi_data_atom extends MY_Model {

	/* Config dasar yang diturunkan ke seluruh model yang mengextend */
	protected $_table_name = 'realisasi_data_atom';
	protected $_primary_key = 'realisasi_id';
	protected $_group_by = '';
	protected $_order_by = '';
	protected $_sequence_name = 'realisasi_data_atom_realisasi_id_seq';

	/* Fungsi Construct (wajib ada) */
	public function __construct()
	{
		parent::__construct();
		// $this->db->reset_query();
	}

}

/* End of file Realisasi_data_atom.php */
/* Location: ./application/models/Realisasi_data_atom.php */