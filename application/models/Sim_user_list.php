<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sim_user_list extends MY_Model {

	/* Config dasar yang diturunkan ke seluruh model yang mengextend */
	protected $_table_name = 'sim_user_list';
	protected $_primary_key = 'user_id';
	protected $_group_by = '';
	protected $_order_by = '';
	protected $_sequence_name = 'sim_user_list_user_id_seq';

	/* Fungsi Construct (wajib ada) */
	public function __construct()
	{
		parent::__construct();
		// $this->db->reset_query();
	}

}

/* End of file Sim_user_list.php */
/* Location: ./application/models/Sim_user_list.php */