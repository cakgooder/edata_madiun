<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sim_setting extends MY_Model {

	/* Config dasar yang diturunkan ke seluruh model yang mengextend */
	protected $_table_name = 'sim_setting';
	protected $_primary_key = 'setting_kode';
	protected $_group_by = '';
	protected $_order_by = '';
	protected $_sequence_name = '';

	/* Fungsi Construct (wajib ada) */
	public function __construct()
	{
		parent::__construct();
		// $this->db->reset_query();
	}

}

/* End of file Sim_setting.php */
/* Location: ./application/models/Sim_setting.php */