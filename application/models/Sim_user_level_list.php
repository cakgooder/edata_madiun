<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sim_user_level_list extends MY_Model {

	/* Config dasar yang diturunkan ke seluruh model yang mengextend */
	protected $_table_name = 'sim_user_level_list';
	protected $_primary_key = 'user_level';
	protected $_group_by = '';
	protected $_order_by = '';
	protected $_sequence_name = '';

	/* Fungsi Construct (wajib ada) */
	public function __construct()
	{
		parent::__construct();
		// $this->db->reset_query();
	}

}

/* End of file Sim_user_level_list.php */
/* Location: ./application/models/Sim_user_level_list.php */