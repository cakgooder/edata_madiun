<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_kolom_data_master extends MY_Model {

	/* Config dasar yang diturunkan ke seluruh model yang mengextend */
	protected $_table_name = 'master_kolom_data_master';
	protected $_primary_key = 'data_master_id';
	protected $_group_by = '';
	protected $_order_by = '';
	protected $_sequence_name = '';

	/* Fungsi Construct (wajib ada) */
	public function __construct()
	{
		parent::__construct();
		// $this->db->reset_query();
	}

}

/* End of file Master_kolom_data_master.php */
/* Location: ./application/models/Master_kolom_data_master.php */