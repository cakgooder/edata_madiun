<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if (!$this->session->has_userdata('user_level')) {
			redirect('login');
		}
	}

	/* Halaman Index */
	public function index()
	{
		/* Get semua daftar user */
		$where_user["user_id <>"] = $this->session->userdata('user_id');
		$this->data["daftar_user"] = $this->sim_user_list->get_by($where_user, "result_array");

		/* Set page config */
		$this->_set_config_halaman("Setting User", "Setting User", "", "setting-user");

		/* Load Dashboard */
		$this->_load_view_dashboard_all("setting/user/index_data");
	}

	/* Halaman Tambah */
	public function tambah()
	{
		/* Set page config */
		$this->_set_config_halaman("Setting User", "Tambah User", "", "setting-user");

		/* Load Dashboard */
		$this->_load_view_dashboard_all("setting/user/tambah_data");
	}

	/* Halaman Edit */
	public function edit($user_id_enc = "")
	{
		/* Lakukan pengecekan id yang dikirim apakah kosong */
		if ($user_id_enc == "") {
			/* Set messages */
			$this->_flashdata_notif("Anda mencoba mengakses halaman dengan cara yang salah !!!", "error");

			/* Redirect ke halaman yang benar */
			redirect('dashboard/setting/user');
		}

		/* Decrypt id */
		$user_id = decrypting_code($user_id_enc);

		/* Get dan cek data */
		$where_user["user_id"] = $user_id;
		$data_user = $this->sim_user_list->get_by($where_user, "row_array");

		/* Pengecekan apakah hasil null */
		if (empty($data_user)) {
			/* Set messages */
			$this->_flashdata_notif("ID yang anda ingin edit tidak terdaftar !!!", "error");

			/* Redirect ke halaman yang benar */
			redirect('dashboard/setting/user');
		}

		/* Masukkan ke database semua data user */
		$this->data["data_user"] = $data_user;

		/* Set page config */
		$this->_set_config_halaman("Setting User", "Edit User", "", "setting-user");

		/* Load Dashboard */
		$this->_load_view_dashboard_all("setting/user/edit_data");
	}

	/* Halaman Proses Tambah */
	public function proses_tambah()
	{
		/* Get semua data POST */
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$user_nama = $this->input->post('user_nama');
		$user_level = $this->input->post('user_level');

		/* Pengecekan data kosong */
		if (empty($username) || empty($user_nama) || empty($password)) {
			/* Set messages */
			$this->_flashdata_notif("Data yang anda masukkan masih ada yang kosong !!!", "error");

			/* Redirect ke halaman yang benar */
			redirect('dashboard/setting/user/tambah');
		}

		/* Masukkan ke data kirim */
		$kirim_user["username"] = underscore($username);
		$kirim_user["password"] = do_hash($password, "md5");
		$kirim_user["user_nama"] = $user_nama;
		$kirim_user["user_level"] = $user_level;

		/* Masukkan ke database */
		$this->sim_user_list->save($kirim_user);

		/* Set messages */
		$this->_flashdata_notif("Anda berhasil tambah User Baru");

		/* Redirect ke halaman yang benar */
		redirect('dashboard/setting/user');
	}

	/* Halaman Proses Edit */
	public function proses_edit($user_id_enc = "")
	{
		/* Lakukan pengecekan id yang dikirim apakah kosong */
		if ($user_id_enc == "") {
			/* Set messages */
			$this->_flashdata_notif("Anda mencoba mengakses halaman dengan cara yang salah !!!", "error");

			/* Redirect ke halaman yang benar */
			redirect('dashboard/setting/user');
		}

		/* Decrypt id */
		$user_id = decrypting_code($user_id_enc);

		/* Get semua data POST */
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$user_nama = $this->input->post('user_nama');
		$user_level = $this->input->post('user_level');

		/* Pengecekan password */
		if (!empty($password)) {
			$kirim_user["password"] = do_hash($password, "md5");
		}

		/* Masukkan ke data kirim */
		$kirim_user["username"] = underscore($username);
		$kirim_user["user_nama"] = $user_nama;
		$kirim_user["user_level"] = $user_level;

		/* Masukkan ke database */
		$this->sim_user_list->save($kirim_user, $user_id);

		/* Set messages */
		$this->_flashdata_notif("Anda berhasil Edit data User");

		/* Redirect ke halaman yang benar */
		redirect('dashboard/setting/user');
	}

	/* Halaman Proses Hapus */
	public function proses_hapus()
	{
		/* Get data POST */
		$user_id_enc = $this->input->post('user_id_enc');

		/* Decrypt id */
		$user_id = decrypting_code($user_id_enc);

		/* Check if not NULL */
		$data_json = [];
		if (empty($user_id)) {
			/* Return json */
			$data_json["status"] = "failed";
			$data_json["messages"] = "User tidak ditemukan!!";
		} else {
			/* Find user */
			$where_del["user_id"] = $user_id;
			$data_user = $this->sim_user_list->get_by($where_del, "row_array");

			if (empty($data_user)) {
				/* Return json */
				$data_json["status"] = "failed";
				$data_json["messages"] = "User tidak ditemukan!!";
				
			} else {
				/* Delete user */
				$this->sim_user_list->delete($where_del);

				/* Return json */
				$data_json["user_id_deleted"] = $user_id;
				$data_json["status"] = "success";
				$data_json["messages"] = "User ".$data_user["user_nama"]." berhasil dihapus";
			}
		}

		/* Load view Dashboard */
		echo json_encode($data_json);
	}

	/* Halaman Proses Lock */
	public function proses_lock()
	{
		/* Get data POST */
		$user_id = $this->input->post('user_id');
		$status_lock = $this->input->post('status');

		/* Check if not NULL */
		$data_json = [];
		if (empty($user_id) || empty($status_lock)) {
			/* Return json */
			$data_json["status"] = "failed";
			$data_json["messages"] = "User tidak ditemukan!!";
		} else {
			/* Find user */
			$where_user["user_id"] = $user_id;
			$data_user = $this->sim_user_list->get_by($where_user, "row_array");

			if (empty($data_user)) {
				/* Return json */
				$data_json["status"] = "failed";
				$data_json["messages"] = "User tidak ditemukan!!";
				
			} else {
				/* Change status user */
				$kirim_user["locked"] = $status_lock;
				$this->sim_user_list->save($kirim_user, $user_id);

				/* Return json */
				$data_json["user_id"] = $user_id;
				$data_json["status"] = "success";
				if ($status_lock == "locked") {
					$data_json["messages"] = "User ".$data_user["user_nama"]." berhasil dikunci";
				} else if($status_lock == "unlocked") {
					$data_json["messages"] = "User ".$data_user["user_nama"]." berhasil buka kunci";
				}
			}
		}

		/* Load view Dashboard */
		echo json_encode($data_json);
	}

}

/* End of file User.php */
/* Location: ./application/controllers/dashboard/setting/User.php */