<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tahun_data extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if (!$this->session->has_userdata('user_level')) {
			redirect('login');
		}
	}

	public function index()
	{
		/* Setting Page */
		$this->_set_config_halaman("Setting Tahun Data", "Setting Tahun Data", "", "setting-tahun-data");

		/* Load Dashboard */
		$this->_load_view_dashboard_all("setting/tahun_data/index_data");
	}

	/* Fungsi Simpan */
	public function simpan()
	{
		/* Get data POST */
		$tahun_mulai = $this->input->post('tahun_mulai');
		$tahun_berakhir = $this->input->post('tahun_berakhir');

		/* Simpan settingan */
		$kirim_setting["setting_value"] = $tahun_mulai;
		$this->sim_setting->save($kirim_setting, "tahun_mulai");
		$kirim_setting["setting_value"] = $tahun_berakhir;
		$this->sim_setting->save($kirim_setting, "tahun_berakhir");

		/* Set messages */
		$this->_flashdata_notif("Settingan berhasil disimpan");

		/* Redirect kembali */
		redirect('dashboard/setting/tahun-data');
	}

}

/* End of file Tahun_data.php */
/* Location: ./application/controllers/dashboard/setting/Tahun_data.php */