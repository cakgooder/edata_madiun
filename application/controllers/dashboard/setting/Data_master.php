<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_master extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if (!$this->session->has_userdata('user_level')) {
			redirect('login');
		}
	}

	public function index()
	{
		/* Get data */
		$list_urusan = $this->master_urusan_list->get("result_array");

		/* Set Data Pusat */
		$this->data["list_urusan"] = $list_urusan;

		/* Set page config */
		$this->_set_config_halaman("Data Master", "Setting Data Master", "", "setting-data-master");

		/* Load Page */
		$this->_load_view_dashboard_all("setting/data_master/index_data");
	}

	/* Filter per Urusan */
	public function proses_filter_urusan()
	{
		/* Get data kirim */
		$urusan_kode = $this->input->post('urusan_kode');

		/* Set session pencarian */
		$array = array(
			'urusan_kode' => $urusan_kode
		);
		$this->session->set_userdata( $array );

		/* Set where */
		$where_urusan["urusan_kode"] = $urusan_kode;
		$where_urusan["data_master_status ="] = "active";
		$list_data_master = $this->master_data_master_list->get_by($where_urusan, "result_array");

		/* Set Data Pusat */
		$this->data["list_data_master"] = $list_data_master;

		/* Load only view */
		$this->_load_only_view_all("setting/data_master/filter_urusan");
	}

	/* Tambah Data Master */
	public function tambah()
	{
		/* Get data */
		$list_urusan = $this->master_urusan_list->get("result_array");

		/* Set Data Pusat */
		$this->data["list_urusan"] = $list_urusan;

		/* Set page config */
		$this->_set_config_halaman("Data Master", "Tambah Data Master", "Tambah Data Master", "setting-data-master");

		/* Load Page */
		$this->_load_view_dashboard_all("setting/data_master/tambah_data");
	}

	/* Halaman Edit */
	public function edit($data_master_id_enc = "")
	{
		/* Lakukan pengecekan id yang dikirim apakah kosong */
		if (empty($data_master_id_enc)) {
			/* Set messages */
			$this->_flashdata_notif("Anda mencoba mengakses halaman dengan cara yang salah !!!", "error");

			/* Redirect */
			redirect('dashboard/setting/data-master');
		}

		/* Decrypt */
		$data_master_id = decrypting_code($data_master_id_enc);

		/* Get Data Master */
		$where_data_master["data_master_id"] = $data_master_id;
		$row_data_master = $this->master_data_master_list->get_by($where_data_master, "row_array");

		/* Get daftar kolom */
		$daftar_kolom = $this->master_kolom_data_master->get_by($where_data_master, "result_array");

		/* Masukkan ke data pusat */
		$last_data = end($daftar_kolom);
		$this->data["row_data_master"] = $row_data_master;
		$this->data["daftar_kolom"] = $daftar_kolom;
		$this->data["index_kolom"] = $last_data["kolom_ke"] + 1;

		/* Load Tipe Data dari config */
		$this->data["daftar_tipe_data"] = $this->config->item('type_data');

		/* Get semua urusan */
		$this->data["daftar_urusan"] = $this->master_urusan_list->get("result_array");

		/* Setting Page */
		$this->_set_config_halaman("Setting Data Master", "Setting Data Master", "Edit Data Master", "setting-data-master");

		/* Load Dashboard */
		$this->_load_view_dashboard_all("setting/data_master/edit_data");
	}

	public function tambah_kolom($kolom_key = 1)
	{
		/* Load Tipe Data dari config */
		$this->data["daftar_tipe_data"] = $this->config->item('type_data');

		/* Masukkan kolom key */
		$this->data["kolom_key"] = $kolom_key;

		/* Load Only Page */
		$this->_load_only_view_all("setting/data_master/tambah_kolom");
	}

	/* Halaman Proses Tambah */
	public function proses_tambah()
	{
		/* Get data POST */
		$urutan_kolom_key = $this->input->post('urutan_kolom_key');
		$data_master_nama = $this->input->post('data_master_nama');
		$urusan_kode = $this->input->post('urusan_kode');
		$data_kolom = $this->input->post('data_kolom');
		$data_master_ket = $this->input->post('data_master_ket');

		/* Buat Data Master Baru */
		$kirim_data_master["data_master_nama"] = $data_master_nama;
		$kirim_data_master["urusan_kode"] = $urusan_kode;
		$kirim_data_master["data_master_ket"] = $data_master_ket;
		$data_master_id = $this->master_data_master_list->save($kirim_data_master);

		/* Get ID Data Master */
		$row_data_master = $this->master_data_master_list->get_by($kirim_data_master, "row_array");

		/* Rubah urutan menjadi array */
		$urutan_kolom_key = explode(";", $urutan_kolom_key);

		/* Deklarasi Kolom ke sebagai acuan */
		$kolom_ke = 1;

		/* Looping sesuai urutan */
		foreach ($urutan_kolom_key as $urutan_ke) {
			/* Check apakah urutan_ke ada di data_kolom */
			if ($urutan_ke != "" || $urutan_ke != NULL) {
				$kirim_kolom = [];
				/* Set Kirim */
				$kirim_kolom["data_master_id"] = $data_master_id;
				$kirim_kolom["kolom_ke"] = $kolom_ke;
				$kirim_kolom["kolom_nama"] = $data_kolom[$urutan_ke]["kolom_nama"];
				$kirim_kolom["kolom_kode"] = $data_kolom[$urutan_ke]["kolom_kode"];
				$kirim_kolom["kolom_type_data"] = $data_kolom[$urutan_ke]["kolom_type_data"];
				$kirim_kolom["kolom_ket"] = $data_kolom[$urutan_ke]["kolom_ket"];
				$kirim_kolom["kolom_boolean"] = $data_kolom[$urutan_ke]["kolom_boolean"];

				/* Masukkan ke database */
				$this->master_kolom_data_master->save($kirim_kolom);

				/* Increment Kolom ke */
				$kolom_ke++;
			}
		}

		/* Set messages */
		$this->_flashdata_notif("Berhasil Tambah Data Master Baru", "success");

		/* Redirect */
		redirect('dashboard/setting/data-master');
	}

	/* Halaman Proses Edit */
	public function proses_edit($data_master_enc_id = "")
	{
		/* Lakukan pengecekan id yang dikirim apakah kosong */
		if (empty($data_master_enc_id)) {
			/* Set messages */
			$this->_flashdata_notif("Anda mencoba mengakses halaman dengan cara yang salah !!!", "error");

			/* Redirect */
			redirect('dashboard/setting/data-master');
		}

		/* Decrypt */
		$data_master_id = decrypting_code($data_master_enc_id);

		/* Get data POST */
		$urutan_kolom_key = $this->input->post('urutan_kolom_key');
		$data_master_nama = $this->input->post('data_master_nama');
		$urusan_kode = $this->input->post('urusan_kode');
		$data_kolom = $this->input->post('data_kolom');
		$data_master_ket = $this->input->post('data_master_ket');

		/* Hapus Kolom Sebelumnya */
		$where_kolom["data_master_id"] = $data_master_id;
		$this->master_kolom_data_master->delete($where_kolom);

		/* Rubah urutan menjadi array */
		$urutan_kolom_key = explode(";", $urutan_kolom_key);

		/* Deklarasi Kolom ke sebagai acuan */
		$kolom_ke = 1;

		/* Looping sesuai urutan */
		foreach ($urutan_kolom_key as $urutan_ke) {
			/* Check apakah urutan_ke ada di data_kolom */
			if ($urutan_ke != "" || $urutan_ke != NULL) {
				$kirim_kolom = [];
				/* Set Kirim */
				$kirim_kolom["data_master_id"] = $data_master_id;
				$kirim_kolom["kolom_ke"] = $kolom_ke;
				$kirim_kolom["kolom_nama"] = $data_kolom[$urutan_ke]["kolom_nama"];
				$kirim_kolom["kolom_kode"] = $data_kolom[$urutan_ke]["kolom_kode"];
				$kirim_kolom["kolom_type_data"] = $data_kolom[$urutan_ke]["kolom_type_data"];
				$kirim_kolom["kolom_ket"] = $data_kolom[$urutan_ke]["kolom_ket"];
				$kirim_kolom["kolom_boolean"] = $data_kolom[$urutan_ke]["kolom_boolean"];

				/* Masukkan ke database */
				$this->master_kolom_data_master->save($kirim_kolom);

				/* Increment Kolom ke */
				$kolom_ke++;
			}
		}

		/* Edit Data Master Baru */
		$kirim_data_master["urusan_kode"] = $urusan_kode;
		$kirim_data_master["data_master_nama"] = $data_master_nama;
		$kirim_data_master["data_master_ket"] = $data_master_ket;
		$kirim_data_master["data_master_sesuai_excel"] = "queue";
		$this->master_data_master_list->save($kirim_data_master, $data_master_id);

		/* Set messages */
		$this->_flashdata_notif("Berhasil Edit Data Master", "success");

		/* Redirect */
		redirect('dashboard/setting/data-master');
	}

	/* Proses Hapus Data Master */
	public function proses_hapus()
	{
		/* Get POST */
		$data_master_id_enc = $this->input->post('data_master_id');

		/* Decrypt */
		$data_master_id = decrypting_code($data_master_id_enc);

		/* Set Not Active */
		$kirim_data_master["data_master_status"] = "inactive";
		$this->master_data_master_list->save($kirim_data_master, $data_master_id);

		/* Messages */
		$result["data_master_id_deleted"] = $data_master_id;
		$result["status"] = "success";
		$result["messages"] = "Berhasil Hapus Data Master ".$data_master_id;

		/* Send output */
		echo json_encode($result);
	}

}

/* End of file Data_master.php */
/* Location: ./application/controllers/dashboard/setting/Data_master.php */