<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_atom extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if (!$this->session->has_userdata('user_level')) {
			redirect('login');
		}
	}

	public function index()
	{
		/* Get semua urusan */
		$this->get_urusan_master_data();

		/* Setting Page */
		$this->_set_config_halaman("Setting Data Atom", "Setting Data Atom", "Daftar Data Atom", "setting-data-atom");

		/* Load Dashboard */
		$this->_load_view_dashboard_all("setting/data_atom/index_data");
	}

	/* Halaman Filter Data Master */
	public function filter_data_master()
	{
		/* get data POST */
		$data_master_id_enc = $this->input->post('data_master_id_enc');

		/* Decrypt */
		$data_master_id = decrypting_code($data_master_id_enc);

		/* Get Data Atom dari Data Master */
		$where_data_atom["data_master_id"] = $data_master_id;
		$where_data_atom["data_atom_status"] = "active";
		$daftar_data_atom = $this->master_data_atom_list->get_by($where_data_atom, "result_array", "data_atom_narasi");

		/* Set data pusat */
		$this->data["daftar_data_atom"] = $daftar_data_atom;

		/* Load Only Page */
		$this->_load_only_view_all("setting/data_atom/filter_data_master");
	}

	/* Halaman Tambah Data Atom */
	public function tambah()
	{
		/* Get semua urusan */
		$this->get_urusan_master_data();

		/* Setting Page */
		$this->_set_config_halaman("Setting Data Atom", "Setting Data Atom", "Tambah Data Atom", "setting-data-atom");

		/* Load Dashboard */
		$this->_load_view_dashboard_all("setting/data_atom/tambah_data");
	}

	/* Halaman Edit Data Atom */
	public function edit($data_atom_id_enc)
	{
		/* Lakukan pengecekan id yang dikirim apakah kosong */
		if (empty($data_atom_id_enc)) {
			/* Set messages */
			$this->_flashdata_notif("Anda mencoba mengakses halaman dengan cara yang salah !!!", "error");

			/* Redirect */
			redirect('dashboard/setting/data-atom');
		}

		/* Decrypt */
		$data_atom_id = decrypting_code($data_atom_id_enc);

		/* Get Data Atom dari Data Master */
		$where_data_atom["data_atom_id"] = $data_atom_id;
		$detail_data_atom = $this->master_data_atom_list->get_by($where_data_atom, "row_array", "data_atom_narasi");

		/* Get semua urusan */
		$this->get_urusan_master_data();

		/* Get kolom dari Data Master */
		$where_kolom["data_master_id"] = $detail_data_atom["data_master_id"];
		$daftar_kolom = $this->master_kolom_data_master->get_by($where_kolom, "result_array", "kolom_ke");

		/* Get jenis realisasi */
		$daftar_jenis_realisasi = $this->config->item('type_realisasi');

		/* Set data pusat */
		$this->data["detail_data_atom"] = $detail_data_atom;
		$this->data["daftar_kolom"] = $daftar_kolom;
		$this->data["daftar_jenis_realisasi"] = $daftar_jenis_realisasi;

		/* Setting Page */
		$this->_set_config_halaman("Setting Data Atom", "Setting Data Atom", "Edit Data Atom", "setting-data-atom");

		/* Load Dashboard */
		$this->_load_view_dashboard_all("setting/data_atom/edit_data");
	}

	/* Proses Tambah */
	public function proses_tambah()
	{
		/* Get semua data POST */
		$data_atom_narasi = $this->input->post('data_atom_narasi');
		$data_master_id = $this->input->post('data_master_id');
		$data_atom_satuan = $this->input->post('data_atom_satuan');
		$data_atom_kolom_realisasi_awal = $this->input->post('data_atom_kolom_realisasi_awal');
		$data_atom_kolom_realisasi_akhir = $this->input->post('data_atom_kolom_realisasi_akhir');
		$data_atom_jenis_realisasi = $this->input->post('data_atom_jenis_realisasi');

		/* Set Kirim */
		$kirim_data_atom["data_atom_narasi"] = $data_atom_narasi;
		$kirim_data_atom["data_master_id"] = $data_master_id;
		$kirim_data_atom["data_atom_satuan"] = $data_atom_satuan;
		$kirim_data_atom["data_atom_kolom_realisasi_awal"] = $data_atom_kolom_realisasi_awal;
		if (!empty($data_atom_kolom_realisasi_akhir)) {
			$kirim_data_atom["data_atom_kolom_realisasi_akhir"] = $data_atom_kolom_realisasi_akhir;
		}
		$kirim_data_atom["data_atom_jenis_realisasi"] = $data_atom_jenis_realisasi;
		$data_atom_id = $this->master_data_atom_list->save($kirim_data_atom);

		/* Simpan Filter */

		/* Set messages */
		$this->_flashdata_notif("Berhasil Tambah Data Atom Baru", "success");

		/* Redirect */
		redirect('dashboard/setting/data-atom');
	}

	/* Proses Edit */
	public function proses_edit($data_atom_id_enc)
	{
		/* Lakukan pengecekan id yang dikirim apakah kosong */
		if (empty($data_atom_id_enc)) {
			/* Set messages */
			$this->_flashdata_notif("Anda mencoba mengakses halaman dengan cara yang salah !!!", "error");

			/* Redirect */
			redirect('dashboard/setting/data-atom');
		}

		/* Decrypt */
		$data_atom_id = decrypting_code($data_atom_id_enc);

		/* Get semua data POST */
		$data_atom_narasi = $this->input->post('data_atom_narasi');
		$data_master_id = $this->input->post('data_master_id');
		$data_atom_satuan = $this->input->post('data_atom_satuan');
		$data_atom_kolom_realisasi_awal = $this->input->post('data_atom_kolom_realisasi_awal');
		$data_atom_kolom_realisasi_akhir = $this->input->post('data_atom_kolom_realisasi_akhir');
		$data_atom_jenis_realisasi = $this->input->post('data_atom_jenis_realisasi');

		/* Set Kirim */
		$kirim_data_atom["data_atom_narasi"] = $data_atom_narasi;
		$kirim_data_atom["data_master_id"] = $data_master_id;
		$kirim_data_atom["data_atom_satuan"] = $data_atom_satuan;
		$kirim_data_atom["data_atom_kolom_realisasi_awal"] = $data_atom_kolom_realisasi_awal;
		if (!empty($data_atom_kolom_realisasi_akhir)) {
			$kirim_data_atom["data_atom_kolom_realisasi_akhir"] = $data_atom_kolom_realisasi_akhir;
		}
		$kirim_data_atom["data_atom_jenis_realisasi"] = $data_atom_jenis_realisasi;
		$this->master_data_atom_list->save($kirim_data_atom, $data_atom_id);

		/* Simpan Filter */

		/* Set messages */
		$this->_flashdata_notif("Berhasil Edit Data Atom", "success");

		/* Redirect */
		redirect('dashboard/setting/data-atom');
	}

	/* Proses Hapus */
	public function proses_hapus()
	{
		/* Get data POST */
		$data_atom_id_enc = $this->input->post('data_atom_id');

		/* Decrypt */
		$data_atom_id = decrypting_code($data_atom_id_enc);

		/* Delete data */
		$kirim_data_atom["data_atom_status"] = "inactive";
		$this->master_data_atom_list->save($kirim_data_atom, $data_atom_id);

		/* Insert data ke return */
		$data_return["status"] = "success";
		$data_return["messages"] = "Data Atom Berhasil di hapus";
		$data_return["data_atom_id_deleted"] = $data_atom_id;

		/* Echo json */
		echo json_encode($data_return);
	}

	/* Get filter realisasi dari data master */
	public function get_kolom_data_master()
	{
		/* get data POST */
		$data_master_id = $this->input->post('data_master_id');

		/* Get kolom dari Data Master */
		$where_kolom["data_master_id"] = $data_master_id;
		$daftar_kolom = $this->master_kolom_data_master->get_by($where_kolom, "result_array", "kolom_ke");

		/* Get jenis realisasi */
		$daftar_jenis_realisasi = $this->config->item('type_realisasi');

		/* Masukkan data pusat */
		$this->data["daftar_kolom"] = $daftar_kolom;
		$this->data["daftar_jenis_realisasi"] = $daftar_jenis_realisasi;

		/* Load Only Page */
		$this->_load_only_view_all("setting/data_atom/get_kolom_data_master");
	}

	/* Function get Urusan dan Data Atom */
	private function get_urusan_master_data()
	{
		/* Get semua urusan */
		$daftar_urusan = $this->master_urusan_list->get("result_array");

		/* Looping daftar urusan */
		foreach ($daftar_urusan as $key_urusan => $row_urusan) {
			/* Get daftar data master */
			$where_data_master["urusan_kode"] = $row_urusan["urusan_kode"];
			$where_data_master["data_master_status"] = "active";
			$daftar_data_master = $this->master_data_master_list->get_by($where_data_master, "result_array");

			/* Satukan dengan urusan */
			$daftar_urusan[$key_urusan]["daftar_data_master"] = $daftar_data_master;
		}

		/* Data Pusat */
		$this->data["daftar_urusan"] = $daftar_urusan;
	}

}

/* End of file Data_atom.php */
/* Location: ./application/controllers/dashboard/setting/Data_atom.php */