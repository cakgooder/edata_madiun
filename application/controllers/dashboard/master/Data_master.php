<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_master extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if (!$this->session->has_userdata('user_level')) {
			redirect('login');
		}
	}

	public function index()
	{
		/* Get data */
		$list_urusan = $this->master_urusan_list->get("result_array");

		/* Set Data Pusat */
		$this->data["list_urusan"] = $list_urusan;

		/* Set page config */
		$this->_set_config_halaman("Data Master", "List Data Master", "", "master-data-master");

		/* Load Page */
		$this->_load_view_dashboard_all("master/data_master/index_data");
	}

	/* Filter per Urusan */
	public function proses_filter_urusan()
	{
		/* Get data kirim */
		$urusan_kode = $this->input->post('urusan_kode');

		/* Set where */
		$where_urusan["urusan_kode"] = $urusan_kode;
		$list_data_master = $this->master_data_master_list->get_by($where_urusan, "result_array");

		/* Set Data Pusat */
		$this->data["list_data_master"] = $list_data_master;

		/* Load only view */
		$this->_load_only_view_all("master/data_master/filter_urusan");
	}

}

/* End of file Data_master.php */
/* Location: ./application/controllers/dashboard/master/Data_master.php */