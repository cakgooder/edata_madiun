<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_atom extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if (!$this->session->has_userdata('user_level')) {
			redirect('login');
		}
	}

	public function index()
	{
		redirect("dashboard");
	}

	/* Halaman Grafik */
	public function grafik()
	{
		/* Get Data Atom dari Data Master */
		$where_data_atom["data_atom_status"] = "active";
		$daftar_data_atom = $this->master_data_atom_list->get_by($where_data_atom, "result_array", "data_atom_narasi");

		/* Set data pusat */
		$this->data["daftar_data_atom"] = $daftar_data_atom;

		/* Setting Page */
		$this->_set_config_halaman("Report Data Atom", "Report Grafik Data Atom", "", "report-grafik-data-atom");

		/* Load Dashboard */
		$this->_load_view_dashboard_all("report/data_atom/grafik");
	}

	/* Halaman tambah grafik */
	public function tambah_grafik($data_atom_id_enc)
	{
		/* Set data return */
		$data_return = [];

		/* Decrypt */
		$data_atom_id = decrypting_code($data_atom_id_enc);

		/* Lakukan pengecekan id yang dikirim apakah kosong */
		if (empty($data_atom_id)) {
			/* Set messages */
			$data_return["status"] = "error";
			$data_return["messages"] = "Silahkan pilih Data Atom yang ingin ditampilkan";

			/* Echo json */
			echo json_encode($data_return);
			return;
		}

		/* Get data detail */
		$where_realisasi["data_atom_id"] = $data_atom_id;
		$detail_data_atom = $this->master_data_atom_list->get_by($where_realisasi, "row_array");

		/* Olah data */
		$hasil_olah = $this->olah_data_grafik($detail_data_atom);

		/* Insert data ke return */
		$data_return["status"] = "success";
		$data_return["messages"] = "Data Berhasil di tambahkan ke grafik";
		$data_return["data"]["to_load"] = $hasil_olah;
		$data_return["data"]["detail"] = $detail_data_atom;
		$data_return["data"]["color"] = "#2196F3";

		/* Echo json */
		echo json_encode($data_return);
	}

	/* Fungsi olah data untuk grafik */
	private function olah_data_grafik($detail_data_atom)
	{
		/* Set Nama Kolom */
		$data_return[] = $detail_data_atom["data_atom_narasi"];

		/* Set Realisasi */
		$where_realisasi["data_atom_id"] = $detail_data_atom["data_atom_id"];
		for ($i=$this->data["_tahun_mulai"]; $i <= $this->data["_tahun_berakhir"]; $i++) {
			$where_realisasi["realisasi_tahun_data"] = $i;
			$realisasi_tahun = $this->realisasi_data_atom->get_by($where_realisasi, "row_array");
			if (!empty($realisasi_tahun)) {
				$data_return[] = $realisasi_tahun["realisasi_nilai"];
			} else {
				$data_return[] = 0;
			}
		}

		/* Kembalikan */
		return $data_return;
	}

}

/* End of file Data_atom.php */
/* Location: ./application/controllers/dashboard/report/Data_atom.php */