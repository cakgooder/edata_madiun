<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index()
	{
		/* Get Jumlah Data Master */
		$jml_data_master = $this->master_data_master_list->get("num_rows");

		/* Get Jumlah Data Point */
		$jml_data_atom = $this->master_data_atom_list->get("num_rows");

		/* Insert data pusat */
		$this->data["jml_data_master"] = $jml_data_master;
		$this->data["jml_data_atom"] = $jml_data_atom;

		/* Set page config */
		$this->_set_config_halaman("Dashboard", "Dashboard", "", "dashboard");

		/* Load Page */
		$this->_load_view_dashboard_all();
	}

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/dashboard/Dashboard.php */