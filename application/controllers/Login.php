<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	/* Form Login */
	public function index()
	{
		/* Pengecekan session */
		if ($this->session->has_userdata('user_level')) {
			redirect('dashboard');
		}

		/* Load view Dashboard */
		$this->load->view('view_login', $this->data);
	}

	/* Fungsi logout */
	public function logout()
	{
		/* Destroy session dan redirect ke halaman utama */
		$this->session->sess_destroy();
		redirect('');
	}

	/* Pengecekan Login */
	public function proses()
	{
		/* Get data POST */
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		if (empty($username) || empty($password)) {
			$username = $this->input->get('username');
			$password = $this->input->get('password');
		}

		/* get data POST, jika tidak ada redirect login */
		if ($username == NULL || $username == "") {
			/* Set messages */
			$this->_flashdata_notif("Anda mencoba mengakses melalui jalur yang salah !!!", "error");

			/* Redirect ke login yang benar */
			redirect('login');
		}

		/* Set Where untuk pencarian */
		$where_user['username'] = $username;
		$where_user['password'] = do_hash($password, "md5");

		/* Pencarian username dan password yang sesuai */
		$data_login_array = $this->sim_user_list->get_by($where_user,'row_array');

		/* cek validitas login */
		$log_login = $this->_cek_valid_login($data_login_array);
		if ($log_login['status_login']) {
			/* Set last login */
			$kirim_user["last_login"] = current_timestamp_postgres();
			$this->sim_user_list->save($kirim_user, NULL, $where_user);

			/* Set Session login */
			$this->_set_session($data_login_array);

			/* Set Messages */
			$this->_flashdata_notif("Berhasil Login ".$data_login_array["user_nama"]);

			/* Redirect ke Dashboard */
			redirect('dashboard');

		} else {
			/* Pengecekan jenis kegagalan login */
			if ($log_login['kode'] == '201') {
				/* Set messages */
				$this->_flashdata_notif("Username atau Password salah !!!", "error");
			} else if ($log_login['kode'] == '202') {
				/* Set messages */
				$this->_flashdata_notif("Akun yang anda gunakan belum diaktifkan !!!", "error");
			}

			/* Redirect kembali ke login */
			redirect('login');
		}
	}

	/* Function untuk set session */
	private function _set_session($data_login)
	{
		/* Set userdata login ke dalam session */
		$this->session->set_userdata($data_login);
	}

	/* Function untuk pengecekan validitas login */
	private function _cek_valid_login($data_login = null)
	{
		/* Cek apakah data bukan null atau kosong */
		if ($data_login == null || $data_login == "") {
			$log_login['status_login'] = false;
			$log_login['kode'] = "201";
			return $log_login;
		}

		/* Cek apakah user locked */
		if ($data_login["locked"] == 'locked') {
			$log_login['status_login'] = false;
			$log_login['kode'] = "202";
			return $log_login;
		}

		$log_login['status_login'] = true;
		$log_login['kode'] = "200";
		return $log_login;
	}

}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */