<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migrasi extends CI_Controller {

	public function index()
	{
		/* Load Library */
		$this->load->library('migration');

		/* Set Migration */
		if ($this->migration->current() === FALSE){
			show_error($this->migration->error_string());
		}
	}

}

/* End of file Migrasi.php */
/* Location: ./application/controllers/Migrasi.php */